/**
 *@file:common.js
 *@author:liyacheng
 *
 *@update:2016-08-04
 */
$(function(){
    $(".nav li > ul").each(function(){
        var width1=$(this).parent().width()-3;
        $(this).css("width",width1);
    });
});

function today() {
    var today, yy, mm, dd, ww, weekday;
    today = new Date();
    yy = today.getFullYear();
    mm = today.getMonth() + 1;
    dd = today.getDate();
    ww = today.getDay();
    if(ww == 0) ww = 7;
    var arr = ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"];
    weekday = arr[ww - 1];
    if (mm < 10) mm = "0" + mm;
    if (dd < 10) dd = "0" + dd;
    $("#today").html(weekday + "&nbsp;&nbsp;" + yy + "-" + mm + "-" + dd + "");
}
//日期
$(function(){
    today();
    setInterval('today()',1000);

});

//tab切换通用
$(function(){
    $(".home-tabsNav li").click(function(){
        var ind=$(".home-tabsNav li").index($(this));
        $(this).addClass("home-navActive").siblings().removeClass("home-navActive");
        $(".home-tabsBd .home-tabPanel").eq(ind).addClass("home-tabPanelActive").siblings().removeClass("home-tabPanelActive");
    });
});


//重点资源入口左右滚动
var flag = "left";
function DY_scroll1(wraper1, prev1, next1, list1, speed1) {
    var wraper1 = $(wraper1);
    var prev1 = $(prev1);
    var next1 = $(next1);
    var list1 = $(list1).children('div');
    var w1 = list1.find('ul').outerWidth(true);
    var s1 = speed1;
    next1.click(function () {
        list1.animate({'margin-left': -w1}, function () {
            list1.find('ul').eq(0).appendTo(list1);
            list1.css({'margin-left': 0});
        });
        flag = "left";
    });
    prev1.click(function () {
        list1.find('ul:last').prependTo(list1);
        list1.css({'margin-left': -w1});
        list1.animate({'margin-left': 0});
        flag = "right";
    });
}
DY_scroll1('.home-resourceList_content', '.home-resourceList_leftbtn', '.home-resourceList_rightbtn', '.home-resourceList_content1', 3);


//图书推荐、新刊简目左右滚动
function DY_scroll(wraper, speed) {
    var wraper = $(wraper);
    var prev = wraper.find("div.home-bookList_leftbtn");
    var next = wraper.find("div.home-bookList_rightbtn");
    var img = wraper.find("div.home-bookList_content1").children('ul');
    var w1 = img.find('li').outerWidth(true);
    var s1 = speed;
    next.click(function () {
        img.animate({'margin-left': -w1}, function () {
            img.find('li').eq(0).appendTo(img);
            img.css({'margin-left': 0});
        });
        flag = "left";
    });
    prev.click(function () {
        img.find('li:last').prependTo(img);
        img.css({'margin-left': -w1});
        img.animate({'margin-left': 0});
        flag = "right";
    });
}
DY_scroll('.home-tabPanelActive .home-bookList_content', 3);
DY_scroll('.home-tabPanelActiveNext .home-bookList_content', 3);


//部门介绍弹出层
$(function(){
    $(".leadership").click(function(){
        var ind=$(".leadership").index(this);
        $(".footer").after("<div class='mark'></div>");
        $(".popup").eq(ind).show();
    });
    $(".popup-close").click(function(){
        $(".mark").remove();
        $(this).parents(".popup").hide();
    });
});
