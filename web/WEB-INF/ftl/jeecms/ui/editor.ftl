<#--
<textarea name="textarea"></textarea>
-->
<#macro editor
	name value="" height="230"
	fullPage="false" toolbarSet="My"
	label="" noHeight="false" required="false" colspan="" width="100" help="" helpPosition="2" colon=":" hasColon="true"
	maxlength="65535"
	onclick="" ondblclick="" onmousedown="" onmouseup="" onmouseover="" onmousemove="" onmouseout="" onfocus="" onblur="" onkeypress="" onkeydown="" onkeyup="" onselect="" onchange=""
	>
<#include "control.ftl"/><#rt/>
<#--
<textarea id="${name}" name="${name}">${value}</textarea>  
-->
<script id="${name}" name="${name}" type="text/plain">${value}</script>
<script type="text/javascript">
  <#if site??>MARK="${site.mark?string('true','false')}";<#else>MARK="true";</#if>
  <#if sessionId??>SID="${sessionId!}";</#if>
  
    
function getIpAndPort() {//获得根目录
    var strFullPath = window.document.location.href;
    var strPath = window.document.location.pathname;
    var pos = strFullPath.indexOf(strPath);
    var prePath = strFullPath.substring(0, pos);
    return prePath;
}
  
$(document).ready(function(){
  
    UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
	UE.Editor.prototype.getActionUrl = function(action) {
	    if (action == 'uploadimage' || action == 'uploadscrawl' ) {
	       return "${base+appBase}/ueditor/upload.do;jsessionid="+SID+"?Type=Image&mark="+MARK;
	    }else if (action == 'upfile') {
	   		return "${base+appBase}/ueditor/upload.do;jsessionid="+SID+"?Type=File";
	    }else if (action == 'uploadvideo') {
	   		return "${base+appBase}/ueditor/upload.do;jsessionid="+SID+"?Type=Media";
	    }else if (action == 'snapscreen') {
	   		return "${base}/snapscreen.svl";
	    }else if (action == 'listimage') {
	   		return "${base+appBase}/ueditor/imageManager.do?picNum=50&insite=false";
	    }else if (action == 'catcher') {
	   		return "${base+appBase}/ueditor/getRemoteImage.do?Type=Image";
	    }
	}
  
  var editor= UE.getEditor('${name}',{
   		imageActionName:"uploadimage" ,
   		imageUrlPrefix:"",
   		fileActionName : "upfile",
   		fileUrlPrefix:"",
   		catcherActionName: "catcher",
   		catcherUrlPrefix:"",
   		imageManagerListPath:"",
   		"imageManagerActionName": "listimage", 
   		snapscreenActionName :"snapscreen",
   		wordImageUrl:"${base+appBase}/ueditor/upload.do?Type=File" ,
   		getMovieUrl:"${base+appBase}/ueditor/getmovie.do",
   		videoActionName: "uploadvideo",
   		videoUrlPrefix:""
   });
   //截图快捷键ctrl+shift+A
   editor.addshortcutkey({
        "snapscreen" : "ctrl+shift+65"
    });
  });

  
</script>

<#include "control-close.ftl"/><#rt/>
</#macro>