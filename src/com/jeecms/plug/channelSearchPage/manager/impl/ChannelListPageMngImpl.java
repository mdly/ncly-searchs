package com.jeecms.plug.channelSearchPage.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.cms.manager.main.ChannelMng;
import com.jeecms.common.page.Pagination;
import com.jeecms.plug.channelSearchPage.dao.ChannelListPageDao;
import com.jeecms.plug.channelSearchPage.manager.ChannelListPageMng;

@Service
@Transactional
public class ChannelListPageMngImpl implements ChannelListPageMng{
	@Autowired
	private ChannelListPageDao channelListPageDao;
	@Override
	public Pagination getChildPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, Integer orderBy,
			String title, int pageNo, int count) {
		Pagination p = channelListPageDao.getChildPageForTag(parentId, hasContentOnly, isleaf, orderBy,title, pageNo, count);
        List cList = this.getChannelListByIds(p.getList(), new ArrayList());
        p.setList(cList);
        return p;
	}

	@Override
	public Pagination getAllPageForTag(Integer siteId, boolean hasContentOnly, Integer orderBy, String title,
			int pageNo, int count) {
		Pagination p = channelListPageDao.getAllPageForTag(siteId, hasContentOnly,  orderBy,title, pageNo, count);
        List cList = this.getChannelListByIds(p.getList(), new ArrayList());
        p.setList(cList);
        return p;
	}
	/**
     * 根据id查询栏目
     *
     * @param list
     * @param cList
     * @return
     */
    private List getChannelListByIds(List list, List cList) {
        if (list != null && list.size() > 0) {
            for (Object obj : list) {
                Channel c =channelMng.findById(Integer.parseInt(String.valueOf(obj)));
                cList.add(c);
            }
        }
        return cList;
    }
    @Autowired
    private ChannelMng channelMng;
}
