package com.jeecms.plug.channelSearchPage.manager;

import com.jeecms.common.page.Pagination;

public interface ChannelListPageMng {

	Pagination getChildPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, Integer orderBy,
			String title, int pageNo, int count);

	Pagination getAllPageForTag(Integer siteId, boolean hasContentOnly, Integer orderBy, String title, int pageNo,
			int count);

}
