package com.jeecms.plug.channelSearchPage.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.common.page.Pagination;
import com.jeecms.plug.channelSearchPage.dao.ChannelListPageDao;

public class ChannelListPageDaoImpl extends HibernateBaseDao<Channel, Integer> implements ChannelListPageDao{

	@Override
	public Pagination getChildPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, Integer orderBy,
			String title, int pageNo, int pageSize) {
		
		String sql=this.getChildSql(parentId, hasContentOnly,isleaf,orderBy, title);
		Session session=this.getSession();
		int totalCount = Integer.parseInt(session.createSQLQuery("select count(1) as count from (" + sql + ") count_t").list().get(0).toString());
        Pagination p = new Pagination(pageNo, pageSize, totalCount);
        if (totalCount < 1) {
            p.setList(new ArrayList());
            return p;
        }
        SQLQuery sQLQuery = session.createSQLQuery(sql);
        List list = sQLQuery.list();
        int startNum = totalCount > p.getFirstResult() ? p.getFirstResult() : totalCount;
        int endNum = totalCount > (p.getFirstResult() + p.getPageSize()) ? (p.getFirstResult() + p.getPageSize()) : totalCount;
        p.setList(list.subList(startNum, endNum));
        return p;
	}

	@Override
	public Pagination getAllPageForTag(Integer siteId, boolean hasContentOnly, Integer orderBy, String title,
			int pageNo, int pageSize) {
		String sql=this.getAllSql(siteId, hasContentOnly, orderBy, title);
		Session session=this.getSession();
		int totalCount = Integer.parseInt(session.createSQLQuery("select count(1) as count from (" + sql + ") count_t").list().get(0).toString());
        Pagination p = new Pagination(pageNo, pageSize, totalCount);
        if (totalCount < 1) {
            p.setList(new ArrayList());
            return p;
        }
        SQLQuery sQLQuery = session.createSQLQuery(sql);
        List list = sQLQuery.list();
        int startNum = totalCount > p.getFirstResult() ? p.getFirstResult() : totalCount;
        int endNum = totalCount > (p.getFirstResult() + p.getPageSize()) ? (p.getFirstResult() + p.getPageSize()) : totalCount;
        p.setList(list.subList(startNum, endNum));
        return p;
	}
	public String getChildSql(Integer parentId, boolean hasContentOnly, boolean isleaf, Integer orderBy,
			String title){
		StringBuffer sb=new StringBuffer();
	    sb.append(" SELECT je.channel_id FROM jc_channel_ext je ");
        sb.append(" LEFT JOIN jc_channel t1 ON je.channel_id=t1.channel_id ");
        sb.append(" WHERE 1=1");
        if (parentId != null) {
            String cids = getSession().createSQLQuery(" select (select getChildChannelList(" + parentId + ") id) from dual ").list().get(0).toString();
            StringBuffer params=new StringBuffer();
            if (StringUtils.isNotBlank(cids)) {
                String _cids[] = cids.split(",");
                for (int i = 0; i < _cids.length; i++){
                	if(i==(_cids.length-1)){
                		params.append(_cids[i]);
                	}else{
                		params.append(_cids[i]+",");
                	}
                }
            }
            sb.append(" AND t1.channel_id in ("+params.toString()+")");
        }
        
        sb.append(" AND ( SELECT count(1) FROM jc_channel WHERE parent_id = t1.channel_id ) = 0");
        sb.append(" AND je.channel_name like '%");
		sb.append(title);
		sb.append("%'");
        if(orderBy==1){
        	sb.append("  ORDER BY je.channel_id asc");
        }
        
	 return sb.toString();
		
	}
	public String getAllSql(Integer siteId, boolean hasContentOnly, Integer orderBy, String title){
		StringBuffer sb=new StringBuffer();
		sb.append("SELECT jc.channel_id ");
		sb.append(" FROM  jc_channel   t LEFT JOIN jc_channel_ext jc on t.channel_id=jc.channel_id");
		sb.append(" WHERE 1=1");
		sb.append(" AND NOT EXISTS ( SELECT * FROM jc_channel t1, jc_channel t2 WHERE t1.channel_id = t2.parent_id AND t.channel_id = t1.channel_id)");
		sb.append(" AND jc.channel_name like '%");
		sb.append(title);
		sb.append("%'");
		if(orderBy==1){
			sb.append("ORDER BY jc.channel_id asc");
		}
		return sb.toString();
		
	}

	@Override
	protected Class<Channel> getEntityClass() {
		// TODO Auto-generated method stub
		return null;
	}
}
