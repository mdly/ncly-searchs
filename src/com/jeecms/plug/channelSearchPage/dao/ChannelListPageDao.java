package com.jeecms.plug.channelSearchPage.dao;

import com.jeecms.common.page.Pagination;

public interface ChannelListPageDao {
	Pagination getChildPageForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, Integer orderBy,
			String title, int pageNo, int pageSize);

	Pagination getAllPageForTag(Integer siteId, boolean hasContentOnly, Integer orderBy, String title, int pageNo,
			int pageSize);
}
