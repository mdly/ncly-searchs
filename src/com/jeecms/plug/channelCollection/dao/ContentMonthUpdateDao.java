package com.jeecms.plug.channelCollection.dao;

public interface ContentMonthUpdateDao {
	String findUpdateSize(Integer parentId,String startDate,String endDate);
}
