package com.jeecms.plug.channelCollection.dao;

import java.util.List;

import com.jeecms.cms.entity.main.Content;

public interface DirectiveContentCollectionDao {
	List<Content> getContentListForTag(Integer userId);
}
