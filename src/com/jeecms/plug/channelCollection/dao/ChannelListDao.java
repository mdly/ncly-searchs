package com.jeecms.plug.channelCollection.dao;

import java.util.List;

import com.jeecms.cms.entity.main.Channel;

public interface ChannelListDao {
public List<Channel> getListChannel(String q);
}
