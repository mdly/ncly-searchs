package com.jeecms.plug.channelCollection.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.plug.channelCollection.dao.ChannelListDao;

/**
 * 主要用来查询所有的channel
 * @author xpy
 *
 */
@Repository
public class ChannelListDaoImpl extends HibernateBaseDao<Channel, Integer> implements ChannelListDao{

	@Override
	public List<Channel> getListChannel(String q) {
		StringBuffer sb=new StringBuffer();
		sb.append("SELECT jc.*");
		sb.append(" FROM  jc_channel   t LEFT JOIN jc_channel_ext jc on t.channel_id=jc.channel_id");
		sb.append(" WHERE 1=1");
		sb.append(" AND NOT EXISTS ( SELECT * FROM jc_channel t1, jc_channel t2 WHERE t1.channel_id = t2.parent_id AND t.channel_id = t1.channel_id)");
		sb.append(" AND jc.channel_name like '%");
		sb.append(q);
		sb.append("%'");
		String sql=sb.toString();
		Session session=this.getSession();
		List<Channel> list=session.createSQLQuery(sql).list();
		return list;
	}

	@Override
	protected Class<Channel> getEntityClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
