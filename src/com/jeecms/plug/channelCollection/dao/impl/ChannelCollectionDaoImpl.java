package com.jeecms.plug.channelCollection.dao.impl;

import org.hibernate.Query;

import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.plug.channelCollection.dao.ChannelCollectionDao;

public class ChannelCollectionDaoImpl extends HibernateBaseDao<Object, Integer> implements ChannelCollectionDao{

	@Override
	protected Class<Object> getEntityClass() {
		return null;
	}

	@Override
	public void insert(Integer userId, Integer channelId) {
		String sql="insert into jc_user_channel_collection(user_id,channel_id) VALUES(:userId,:channelId)";
		Query query=this.getSession().createSQLQuery(sql);
		query.setInteger("userId", userId);
		query.setInteger("channelId", channelId);
		query.executeUpdate();
	}

	@Override
	public void delete(Integer userId, Integer channelId) {
		String sql="delete from jc_user_channel_collection where user_id=? and channel_id=?";
		Query query=this.getSession().createSQLQuery(sql);
		query.setInteger(0, userId);
		query.setInteger(1, channelId);
		query.executeUpdate();
	}
	
}
