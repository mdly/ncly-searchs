package com.jeecms.plug.channelCollection.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.jeecms.cms.entity.main.Content;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.plug.channelCollection.dao.ContentMonthUpdateDao;

@Repository
public class ContentMonthUpdateDaoImpl extends HibernateBaseDao<Content, Integer> implements ContentMonthUpdateDao{
	/**
	 * 指定时间范围内的数据更新量
	 * @param parentId
	 * @param startDate
	 * @param endDate
	 * 
	 */
	@Override
	public String findUpdateSize(Integer parentId, String startDate, String endDate) {
		String sql=this.getSql(parentId, startDate, endDate);
		Session session = getSession();
		String count="0";
		List list=session.createSQLQuery(sql).list();
		if(list!=null&&list.size()>0){
			count=list.size()+" ";
		}
		return count;
	}
	public String getSql(Integer parentId, String startDate, String endDate){
		StringBuffer sb=new StringBuffer();
		    sb.append(" SELECT je.* ");
	        sb.append(" FROM jc_content_channel jc ");
	        sb.append(" LEFT JOIN jc_content_ext je ON jc.content_id = je.content_id ");
	        sb.append(" WHERE 1=1");
	        sb.append(" AND jc.channel_id IN (SELECT t1.channel_id FROM jc_channel t1 ");
	        sb.append("WHERE 1 = 1");
	        if (parentId != null) {
	            String cids = getSession().createSQLQuery(" select (select getChildChannelList(" + parentId + ") id) from dual ").list().get(0).toString();
	            StringBuffer params=new StringBuffer();
	            if (StringUtils.isNotBlank(cids)) {
	                String _cids[] = cids.split(",");
	                for (int i = 0; i < _cids.length; i++){
	                	if(i==(_cids.length-1)){
	                		params.append(_cids[i]);
	                	}else{
	                		params.append(_cids[i]+",");
	                	}
	                }
	            }
	            sb.append(" AND t1.channel_id in ("+params.toString()+")");
	        }
	        
	        sb.append(" AND ( SELECT count(1) FROM jc_channel WHERE parent_id = t1.channel_id ) = 0");
	        sb.append("  )");
	        if(startDate!=null&&endDate!=null){
	        	sb.append(" AND je.release_date BETWEEN '"+startDate+"' AND '"+endDate+"'");
	        }
		return sb.toString();
		
	}
	@Override
	protected Class<Content> getEntityClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
