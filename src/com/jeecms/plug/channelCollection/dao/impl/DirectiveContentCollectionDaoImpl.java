package com.jeecms.plug.channelCollection.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.jeecms.cms.entity.main.Content;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.plug.channelCollection.dao.DirectiveContentCollectionDao;
@Repository
public class DirectiveContentCollectionDaoImpl extends HibernateBaseDao<Content, Integer> implements DirectiveContentCollectionDao{

	@Override
	public List<Content> getContentListForTag(Integer userId) {
		String sql="select content_id from jc_user_collection where user_id="+userId;
		Session session=this.getSession();
		SQLQuery query = session.createSQLQuery(sql);
		query.list();
		return query.list();
	}

	@Override
	protected Class<Content> getEntityClass() {
		return null;
	}

}
