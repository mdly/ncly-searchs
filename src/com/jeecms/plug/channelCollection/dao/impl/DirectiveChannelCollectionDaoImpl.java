package com.jeecms.plug.channelCollection.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.plug.channelCollection.dao.DirectiveChannelCollectionDao;
@Repository
public class DirectiveChannelCollectionDaoImpl extends HibernateBaseDao<Channel, Integer> implements DirectiveChannelCollectionDao{
	/**
	 * 此方法用来根据user_id获取channelid
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List getChannelListForTag(Integer userId) {
		String sql="select channel_id from jc_user_channel_collection where user_id="+userId;
		Session session=this.getSession();
		SQLQuery query = session.createSQLQuery(sql);
		query.list();
		return query.list();
	}

	@Override
	protected Class<Channel> getEntityClass() {
		return null;
	}

}
