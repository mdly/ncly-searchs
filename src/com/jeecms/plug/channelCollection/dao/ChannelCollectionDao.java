package com.jeecms.plug.channelCollection.dao;

public interface ChannelCollectionDao {
	void  insert(Integer userId,Integer channelId);
	void delete(Integer userId,Integer channelId);
}
