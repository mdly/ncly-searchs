package com.jeecms.plug.channelCollection.action.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeecms.plug.channelCollection.manager.ContentMonthUpdateMng;

/**
 * 统计对应栏目下内容的更新量
 * 
 * @author xpy
 *
 */
@Controller
public class ContentMonthUpdateAct {
	@Autowired
	private ContentMonthUpdateMng contentMonthUpdateMng;
	/**
	 * 统计每个月的更新量
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/metadata/contentUpdate.jspx")
	public String contentUpdate(HttpServletRequest request,HttpServletResponse response,Model model) {
		String parentId=request.getParameter("parentId");//获取父栏目id
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String count=contentMonthUpdateMng.findUpdateSize(Integer.valueOf(parentId), startDate, endDate);
		return count;

	}
}
