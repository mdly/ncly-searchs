package com.jeecms.plug.channelCollection.action.front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeecms.core.entity.CmsUser;
import com.jeecms.core.web.util.CmsUtils;
import com.jeecms.plug.channelCollection.manager.ChannelCollectionMng;

/**
 * 收藏处理器
 * 
 * @author Administrator
 *
 */
@Controller
public class CollectionAct {
	@Autowired
	private ChannelCollectionMng channelCollectionMng;
	@ResponseBody
	@RequestMapping(value="/metadata/collect.jspx")
	public String collect(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		CmsUser user = CmsUtils.getUser(request);
		String operation = request.getParameter("operate");
		String channelId = request.getParameter("cId");
		if (operation.equals("1")) {
			// 加入收藏
			channelCollectionMng.insert(user.getId(), Integer.valueOf(channelId));
		} else {
			// 取消收藏
			channelCollectionMng.delete(user.getId(), Integer.valueOf(channelId));
		}
		return operation;

	}
}
