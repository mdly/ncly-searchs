package com.jeecms.plug.channelCollection.manager;

import java.util.List;

import com.jeecms.cms.entity.main.Channel;

public interface DirectiveChannelCollectionMng {
List<Channel> getChannelListForTag(Integer userId);
}
