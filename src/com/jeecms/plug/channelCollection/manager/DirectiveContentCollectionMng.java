package com.jeecms.plug.channelCollection.manager;

import java.util.List;

import com.jeecms.cms.entity.main.Content;

public interface DirectiveContentCollectionMng {
	List<Content> getContentListForTag(Integer userId);
}
