package com.jeecms.plug.channelCollection.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.cms.manager.main.ChannelMng;
import com.jeecms.plug.channelCollection.dao.DirectiveChannelCollectionDao;
import com.jeecms.plug.channelCollection.manager.DirectiveChannelCollectionMng;
@Service
@Transactional
public class DirectiveChannelCollectionMngImpl implements DirectiveChannelCollectionMng{
	@Autowired
	private DirectiveChannelCollectionDao directiveChannelCollectionDao;
	@Autowired
	private ChannelMng channelMng;
	@Override
	public List<Channel> getChannelListForTag(Integer userId) {
		List list=directiveChannelCollectionDao.getChannelListForTag(userId);
		List listChannel=this.getListChannel(list);
		return listChannel;
	}
	private List getListChannel(List list) {
		List listChannel=new ArrayList<>();
		if(list!=null&&list.size()>0){
			for (Object obj : list) {
                Channel c =channelMng.findById(Integer.parseInt(String.valueOf(obj)));
                listChannel.add(c);
            }
		}
		return listChannel;
	}

}
