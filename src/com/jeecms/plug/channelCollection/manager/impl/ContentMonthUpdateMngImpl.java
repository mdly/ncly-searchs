package com.jeecms.plug.channelCollection.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeecms.plug.channelCollection.dao.ContentMonthUpdateDao;
import com.jeecms.plug.channelCollection.manager.ContentMonthUpdateMng;

@Service
@Transactional
public class ContentMonthUpdateMngImpl implements ContentMonthUpdateMng{
	@Autowired
	private ContentMonthUpdateDao  contentMonthUpdateDao;
	@Override
	public String findUpdateSize(Integer parentId, String startDate, String endDate) {
		return contentMonthUpdateDao.findUpdateSize(parentId, startDate, endDate);
	}

}
