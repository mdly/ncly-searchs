package com.jeecms.plug.channelCollection.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeecms.plug.channelCollection.dao.ChannelCollectionDao;
import com.jeecms.plug.channelCollection.manager.ChannelCollectionMng;
@Service
@Transactional
public class ChannelCollectionMngImpl implements ChannelCollectionMng{
	@Autowired
	private ChannelCollectionDao channelCollectionDao;
	@Override
	public void insert(Integer userId, Integer channelId) {
		channelCollectionDao.insert(userId, channelId);
	}

	@Override
	public void delete(Integer userId, Integer channelId) {
		channelCollectionDao.delete(userId, channelId);
	}
	
}
