package com.jeecms.plug.channelCollection.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.cms.entity.main.Content;
import com.jeecms.cms.manager.main.ContentMng;
import com.jeecms.plug.channelCollection.dao.DirectiveContentCollectionDao;
import com.jeecms.plug.channelCollection.manager.DirectiveContentCollectionMng;

@Service
@Transactional
public class DirectiveContentCollectionMngImpl implements DirectiveContentCollectionMng{
	@Autowired
	private DirectiveContentCollectionDao directiveContentCollectionDao;
	@Autowired
	private ContentMng  contentMng;
	@SuppressWarnings("rawtypes")
	@Override
	public List<Content> getContentListForTag(Integer userId) {
		List list= directiveContentCollectionDao.getContentListForTag(userId);
		List listContent=this.getListContent(list);
		return listContent;
	}
	private List getListContent(List list) {
		List listContent=new ArrayList<>();
		if(list!=null&&list.size()>0){
			for (Object obj : list) {
                Content c =contentMng.findById(Integer.parseInt(String.valueOf(obj)));
                listContent.add(c);
            }
		}
		return listContent;
	}
}
