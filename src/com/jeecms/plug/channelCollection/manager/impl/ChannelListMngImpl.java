package com.jeecms.plug.channelCollection.manager.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.plug.channelCollection.dao.ChannelListDao;
import com.jeecms.plug.channelCollection.manager.ChannelListMng;
@Service
@Repository
public class ChannelListMngImpl implements ChannelListMng{
	/**
	 * 查询所有的channel，然后判断是否存在
	 */
	@Override
	public List<Channel> getListChannel(String q) {
		return channelListDao.getListChannel(q);
	}
	@Autowired
	private ChannelListDao channelListDao;
	
}
