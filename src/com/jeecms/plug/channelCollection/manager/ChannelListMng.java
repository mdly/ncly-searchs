package com.jeecms.plug.channelCollection.manager;

import java.util.List;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.common.page.Pagination;

public interface ChannelListMng {
	public List<Channel> getListChannel(String q);

}
