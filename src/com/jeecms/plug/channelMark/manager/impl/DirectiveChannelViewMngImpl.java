package com.jeecms.plug.channelMark.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.cms.manager.main.ChannelMng;
import com.jeecms.plug.channelMark.dao.DirectiveChannelViewDao;
import com.jeecms.plug.channelMark.manager.DirectiveChannelViewMng;
@Service
@Transactional
public class DirectiveChannelViewMngImpl implements DirectiveChannelViewMng{
	@Autowired
	private DirectiveChannelViewDao directiveChannelViewDao;
	@Autowired
	private ChannelMng channelMng;
	public List<Channel> getChildListForTag(Integer parentId, boolean hasContentOnly, boolean isleaf,
			boolean isRecommemd, int orderBy,Integer count) {
		List list=directiveChannelViewDao.getChildListForTag( parentId, hasContentOnly, isleaf, isRecommemd, orderBy,count);
		List cList=new ArrayList<>();
		List<Channel> p =this.getChannelListByIds(list, cList);
		return p;
	}
	/**
     * 根据id查询栏目
     *
     * @param list
     * @param cList
     * @return
     */
    private List getChannelListByIds(List list, List cList) {
        if (list != null && list.size() > 0) {
            for (Object obj : list) {
                Channel c =channelMng.findById(Integer.parseInt(String.valueOf(obj)));
                cList.add(c);
            }
        }
        return cList;
    }

}
