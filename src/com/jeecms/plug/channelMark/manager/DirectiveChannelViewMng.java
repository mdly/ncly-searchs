package com.jeecms.plug.channelMark.manager;

import java.util.List;

import com.jeecms.cms.entity.main.Channel;

public interface DirectiveChannelViewMng {
List<Channel> getChildListForTag(Integer parentId, boolean hasContentOnly, boolean isleaf, boolean isRecommemd, int orderBy,Integer count);
}
