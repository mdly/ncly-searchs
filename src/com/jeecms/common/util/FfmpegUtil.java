package com.jeecms.common.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public final class FfmpegUtil {

	/**
	 * 截取视频中的图片不支持linux平台
	 * @param ffmpegPath
	 *            ffmpeg程序绝对地址
	 * @param videoPath 要截图的视频的绝对地址
	 * @param outImpPath 图像保存的绝对地址
	 */
	public static final void capture(final String ffmpegPath, final String videoPath, final String outImpPath) {

		List<String> commands = new ArrayList<String>();
		commands.add(ffmpegPath);
		commands.add("-i");
		commands.add(videoPath);
		commands.add("-y");
		commands.add("-f");
		commands.add("image2");
		commands.add("-ss");
		commands.add("10");
		commands.add("-t");
		commands.add("0.001");
		commands.add("-s");
		commands.add("350x240");
		commands.add(outImpPath);
		try {
			ProcessBuilder processBuilder = new ProcessBuilder(commands);
			processBuilder.redirectErrorStream(false);
			Process process = processBuilder.start();
			InputStream inputStream = process.getInputStream();
			byte[] data = new byte[1024];
			while (inputStream.read(data) != -1) {
			}
			inputStream.close();
			process.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
