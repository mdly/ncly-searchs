package com.jeecms.core.entity.ext;

import com.jeecms.core.entity.UnifiedUser;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * Created by Administrator on 2016/9/1.
 */

public class UnifiedUserExt extends UnifiedUser {
    /**
     * 借书编号
     */
    private String lendBookNo;

    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 单位名称
     */
    private String companyName;

    /**
     * 设置问题
     */
    private Integer question;

    /**
     * 问题答案
     */
    private String answer;

    /**
     * 军线电话
     */
    private String officialTel;

    /**
     * 手机号
     */
    private String phone;

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getLendBookNo() {
        return lendBookNo;
    }

    public void setLendBookNo(String lendBookNo) {
        this.lendBookNo = lendBookNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getQuestion() {
        return question;
    }

    public void setQuestion(Integer question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOfficialTel() {
        return officialTel;
    }

    public void setOfficialTel(String officialTel) {
        this.officialTel = officialTel;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
