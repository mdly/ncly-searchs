package com.jeecms.core.entity.ext;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/9/1.
 */
public class LendBook implements Serializable {

    private Integer id;

    private String name;

    private String company;

    private String lendBookNo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLendBookNo() {
        return lendBookNo;
    }

    public void setLendBookNo(String lendBookNo) {
        this.lendBookNo = lendBookNo;
    }
}
