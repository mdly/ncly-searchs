package com.jeecms.core.dao.impl;

import java.math.BigInteger;
import java.util.List;

import com.jeecms.core.entity.ext.UnifiedUserExt;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.common.page.Pagination;
import com.jeecms.core.dao.UnifiedUserDao;
import com.jeecms.core.entity.UnifiedUser;

@Repository
public class UnifiedUserDaoImpl extends HibernateBaseDao<UnifiedUser, Integer>
		implements UnifiedUserDao {
	public UnifiedUser getByUsername(String username) {
		return findUniqueByProperty("username", username);
	}

	public List<UnifiedUser> getByEmail(String email) {
		return findByProperty("email", email);
	}

	public int countByEmail(String email) {
		String hql = "select count(*) from UnifiedUser bean where bean.email=:email";
		Query query = getSession().createQuery(hql);
		query.setParameter("email", email);
		return ((Number) query.iterate().next()).intValue();
	}

	public Pagination getPage(int pageNo, int pageSize) {
		Criteria crit = createCriteria();
		Pagination page = findByCriteria(crit, pageNo, pageSize);
		return page;
	}

	public UnifiedUser findById(Integer id) {
		UnifiedUser entity = get(id);
		return entity;
	}

	public UnifiedUser save(UnifiedUser bean) {
		getSession().save(bean);
		return bean;
	}

	public UnifiedUser deleteById(Integer id) {
		UnifiedUser entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	public UnifiedUser saveExt(Integer id,UnifiedUserExt unifiedUserExt) {
		String sql = "update jo_user u set u.lendBookNo =:lendbookno , u.truename =:truename , u.companyname =:companyname , u.question =:question , u.answer =:answer,u.officialTel =:officialTel,u.phone =:phone where u.user_id =:id";
		Query query = getSession().createSQLQuery(sql);
		query.setString("lendbookno",unifiedUserExt.getLendBookNo());
		query.setString("truename",unifiedUserExt.getTrueName());
		query.setString("companyname",unifiedUserExt.getCompanyName());
		query.setInteger("question",unifiedUserExt.getQuestion());
		query.setString("answer",unifiedUserExt.getAnswer());
		query.setString("officialTel",unifiedUserExt.getOfficialTel());
		query.setString("phone",unifiedUserExt.getPhone());
		query.setInteger("id",id);
		query.executeUpdate();
		UnifiedUser user = (UnifiedUser) getSession().get(UnifiedUser.class,id);
		return user;
	}

	@Override
	public UnifiedUserExt getByUsername_v1(String username) {
		String sql = "select u.question as question,u.answer as answer,u.email as email,u.password as password,u.companyname as companyName,u.officialTel as officialTel,u.phone as phone from jo_user u where u.username ='"+username+"'";
		List<UnifiedUserExt> list = getSession().createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(UnifiedUserExt.class)).list();

		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public void pwdReset_v1(String username,String pwd) {
		String sql = "update jo_user u set u.password = '"+pwd +"' where u.username ='"+username+"'";
		getSession().createSQLQuery(sql).executeUpdate();
	}

	@Override
	public Boolean lendBookNo_unique(String lendBookNo) {
		String sql = "select count(u.lendBookNo) from jo_user u where u.lendBookNo ='"+lendBookNo+"'";
		Integer i = ((BigInteger)getSession().createSQLQuery(sql).uniqueResult()).intValue();
		//大于0，则该证号已注册
		if(i>0) {
			return true;
		}else{
			return false;
		}
	}

	@Override
	public Integer editInfo(UnifiedUserExt unifiedUserExt) {
		String sql = "update jo_user u set u.email=:eamil,u.password=:pwd,u.officialTel=:officialTel,u.phone=:phone,u.companyname=:company,u.question=:question,u.answer=:answer where u.username=:username";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setString("eamil",unifiedUserExt.getEmail());
		query.setString("pwd",unifiedUserExt.getPassword());
		query.setString("officialTel",unifiedUserExt.getOfficialTel());
		query.setString("phone",unifiedUserExt.getPhone());
		query.setString("company",unifiedUserExt.getCompanyName());
		query.setInteger("question",unifiedUserExt.getQuestion());
		query.setString("answer",unifiedUserExt.getAnswer());
		query.setString("username",unifiedUserExt.getUsername());
		int i = query.executeUpdate();
		return i;
	}


	@Override
	protected Class<UnifiedUser> getEntityClass() {
		return UnifiedUser.class;
	}
}