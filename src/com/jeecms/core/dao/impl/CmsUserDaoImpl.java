package com.jeecms.core.dao.impl;

import com.jeecms.common.hibernate3.Finder;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.jeecms.common.page.Pagination;
import com.jeecms.common.util.DateFormatUtils;
import com.jeecms.core.dao.CmsUserDao;
import com.jeecms.core.entity.CmsUser;
import com.jeecms.core.entity.ext.LendBook;
import com.jeecms.core.entity.ext.UnifiedUserExt;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class CmsUserDaoImpl extends HibernateBaseDao<CmsUser, Integer>
		implements CmsUserDao {
	public Pagination getPage(String username, String email, Integer siteId,
			Integer groupId, Boolean disabled, Boolean admin, Integer rank,
			int pageNo, int pageSize) {
		Finder f = Finder.create("select bean from CmsUser bean");
		if (siteId != null) {
			f.append(" join bean.userSites userSite");
			f.append(" where userSite.site.id=:siteId");
			f.setParam("siteId", siteId);
		} else {
			f.append(" where 1=1");
		}
		if (!StringUtils.isBlank(username)) {
			f.append(" and bean.username like :username");
			f.setParam("username", "%" + username + "%");
		}
		if (!StringUtils.isBlank(email)) {
			f.append(" and bean.email like :email");
			f.setParam("email", "%" + email + "%");
		}
		if (groupId != null) {
			f.append(" and bean.group.id=:groupId");
			f.setParam("groupId", groupId);
		}
		if (disabled != null) {
			f.append(" and bean.disabled=:disabled");
			f.setParam("disabled", disabled);
		}
		if (admin != null) {
			f.append(" and bean.admin=:admin");
			f.setParam("admin", admin);
		}
		if (rank != null) {
			f.append(" and bean.rank<=:rank");
			f.setParam("rank", rank);
		}
		f.append(" order by bean.id desc");
		return find(f, pageNo, pageSize);
	}
	
	@SuppressWarnings("unchecked")
	public List<CmsUser> getList(String username, String email, Integer siteId,
			Integer groupId, Boolean disabled, Boolean admin, Integer rank) {
		Finder f = Finder.create("select bean from CmsUser bean");
		if (siteId != null) {
			f.append(" join bean.userSites userSite");
			f.append(" where userSite.site.id=:siteId");
			f.setParam("siteId", siteId);
		} else {
			f.append(" where 1=1");
		}
		if (!StringUtils.isBlank(username)) {
			f.append(" and bean.username like :username");
			f.setParam("username", "%" + username + "%");
		}
		if (!StringUtils.isBlank(email)) {
			f.append(" and bean.email like :email");
			f.setParam("email", "%" + email + "%");
		}
		if (groupId != null) {
			f.append(" and bean.group.id=:groupId");
			f.setParam("groupId", groupId);
		}
		if (disabled != null) {
			f.append(" and bean.disabled=:disabled");
			f.setParam("disabled", disabled);
		}
		if (admin != null) {
			f.append(" and bean.admin=:admin");
			f.setParam("admin", admin);
		}
		if (rank != null) {
			f.append(" and bean.rank<=:rank");
			f.setParam("rank", rank);
		}
		f.append(" order by bean.id desc");
		return find(f);
	}

	@SuppressWarnings("unchecked")
	public List<CmsUser> getAdminList(Integer siteId, Boolean allChannel,
			Boolean disabled, Integer rank) {
		Finder f = Finder.create("select bean from CmsUser");
		f.append(" bean join bean.userSites us");
		f.append(" where us.site.id=:siteId");
		f.setParam("siteId", siteId);
		if (allChannel != null) {
			f.append(" and us.allChannel=:allChannel");
			f.setParam("allChannel", allChannel);
		}
		if (disabled != null) {
			f.append(" and bean.disabled=:disabled");
			f.setParam("disabled", disabled);
		}
		if (rank != null) {
			f.append(" and bean.rank<=:rank");
			f.setParam("rank", rank);
		}
		f.append(" and bean.admin=true");
		f.append(" order by bean.id asc");
		return find(f);
	}
	
	public Pagination getAdminsByDepartId(Integer id, int pageNo,int pageSize){
		Finder f = Finder.create("select bean from CmsUser bean ");
		f.append(" where bean.department.id=:departId");
		f.setParam("departId", id);
		f.append(" and bean.admin=true");
		f.append(" order by bean.id asc");
		return find(f,pageNo,pageSize);
	}
	
	public Pagination getAdminsByRoleId(Integer roleId, int pageNo, int pageSize){
		Finder f = Finder.create("select bean from CmsUser");
		f.append(" bean join bean.roles role");
		f.append(" where role.id=:roleId");
		f.setParam("roleId", roleId);
		f.append(" and bean.admin=true");
		f.append(" order by bean.id asc");
		return find(f,pageNo,pageSize);
	}

	public CmsUser findById(Integer id) {
		CmsUser entity = get(id);
		return entity;
	}

	public CmsUser findByUsername(String username) {
		return findUniqueByProperty("username", username);
	}

	public int countByUsername(String username) {
		String hql = "select count(*) from CmsUser bean where bean.username=:username";
		Query query = getSession().createQuery(hql);
		query.setParameter("username", username);
		return ((Number) query.iterate().next()).intValue();
	}
	public int countMemberByUsername(String username) {
		String hql = "select count(*) from CmsUser bean where bean.username=:username and bean.admin=false";
		Query query = getSession().createQuery(hql);
		query.setParameter("username", username);
		return ((Number) query.iterate().next()).intValue();
	}

	public int countByEmail(String email) {
		String hql = "select count(*) from CmsUser bean where bean.email=:email";
		Query query = getSession().createQuery(hql);
		query.setParameter("email", email);
		return ((Number) query.iterate().next()).intValue();
	}

	public CmsUser save(CmsUser bean) {
		getSession().save(bean);
		return bean;
	}

	public CmsUser deleteById(Integer id) {
		CmsUser entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public LendBook findByLendBookNo(String lendBookNo) {
		String sql = "select model.id as id,model.name as name,model.company as company,model.lendBookNo as lendBookNo from jo_lend_book model where model.lendBookNo ='"+lendBookNo+"'";
		List<LendBook> list = new ArrayList<LendBook>();
		list = getSession().createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(LendBook.class)).list();
		if(list.size()>0) {
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public Integer registerMember(UnifiedUserExt unifiedUserExt) {
		if(null!=unifiedUserExt.getEmail()) {
			String sql_2 = "select count(*) from jo_user where email ='" + unifiedUserExt.getEmail() + "'";
			int i_ = ((BigInteger)getSession().createSQLQuery(sql_2).uniqueResult()).intValue();
			if (i_ > 0) {
				//该邮箱已注册
				return 3;
			}
		}

		if(null!=unifiedUserExt.getLendBookNo()) {
			String sql_ = "select count(*) from jo_user where lendBookNo ='" + unifiedUserExt.getLendBookNo() + "'";
			int i_ = ((BigInteger)getSession().createSQLQuery(sql_).uniqueResult()).intValue();
			if (i_ > 0) {
				//该借书证号已注册
				return 2;
			}
		}

		String sql = "insert into jo_user(username,email,password,register_time,register_ip,last_login_time,last_login_ip,lendBookNo,truename,companyname,question,answer,officialTel,phone) values (:username,:email,:password,:register_time,:register_ip,:last_login_time,:last_login_ip,:lendBookNo,:truename,:companyname,:question,:answer,:officialTel,:phone)";
		SQLQuery query = getSession().createSQLQuery(sql);
		query.setString("username",unifiedUserExt.getUsername());
		query.setString("email",unifiedUserExt.getEmail());
		query.setString("password",unifiedUserExt.getPassword());
		query.setString("register_time", DateFormatUtils.formatDate(new Date()));
		query.setString("register_ip",unifiedUserExt.getRegisterIp());
		query.setString("last_login_time",DateFormatUtils.formatDate(new Date()));
		query.setString("last_login_ip",unifiedUserExt.getRegisterIp());
		query.setString("lendBookNo",unifiedUserExt.getLendBookNo());
		query.setString("truename",unifiedUserExt.getTrueName());
		query.setString("companyname",unifiedUserExt.getCompanyName());
		query.setInteger("question",unifiedUserExt.getQuestion());
		query.setString("answer",unifiedUserExt.getAnswer());
		query.setString("officialTel",unifiedUserExt.getOfficialTel());
		query.setString("phone",unifiedUserExt.getPhone());
		int i = query.executeUpdate();
		return i;
	}


	@Override
	protected Class<CmsUser> getEntityClass() {
		return CmsUser.class;
	}
}