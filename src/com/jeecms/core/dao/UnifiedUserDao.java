package com.jeecms.core.dao;

import java.util.List;

import com.jeecms.common.hibernate3.Updater;
import com.jeecms.common.page.Pagination;
import com.jeecms.core.entity.UnifiedUser;
import com.jeecms.core.entity.ext.UnifiedUserExt;

public interface UnifiedUserDao {
	public UnifiedUser getByUsername(String username);

	public List<UnifiedUser> getByEmail(String email);

	public int countByEmail(String email);

	public Pagination getPage(int pageNo, int pageSize);

	public UnifiedUser findById(Integer id);

	public UnifiedUser save(UnifiedUser bean);

	public UnifiedUser updateByUpdater(Updater<UnifiedUser> updater);

	public UnifiedUser deleteById(Integer id);

	UnifiedUserExt getByUsername_v1(String username);

	void pwdReset_v1(String username, String s);

	Boolean lendBookNo_unique(String lendBookNo);

	Integer editInfo(UnifiedUserExt unifiedUserExt);

	UnifiedUser saveExt(Integer id,UnifiedUserExt unifiedUserExt);
}