package com.jeecms.cms.dao.main.impl;

import com.jeecms.cms.dao.main.ContentExtMediaDao;
import com.jeecms.cms.entity.main.ContentExtMedia;
import com.jeecms.common.hibernate3.HibernateBaseDao;

public class ContentExtMediaDaoImpl extends HibernateBaseDao<ContentExtMedia, Integer> implements ContentExtMediaDao {

	@Override
	public ContentExtMedia save(ContentExtMedia contentExtMedia) {
		getSession().save(contentExtMedia);
		return contentExtMedia;
	}

	@Override
	public ContentExtMedia update(ContentExtMedia contentExtMedia) {
		getSession().update(contentExtMedia);
		return contentExtMedia;
	}

	@Override
	public ContentExtMedia findById(Integer id) {
		ContentExtMedia contentExtMedia = super.get(id);
		return contentExtMedia;
	}

	@Override
	public ContentExtMedia deleteById(Integer id) {
		ContentExtMedia contentExtMedia = super.get(id);
		if (contentExtMedia != null) {
			getSession().delete(contentExtMedia);
		}
		return contentExtMedia;
	}

	@Override
	protected Class<ContentExtMedia> getEntityClass() {
		return ContentExtMedia.class;
	}

	@Override
	public void deleteByContentId(Integer contentId) {
		String sql="delete from ContentExtMedia where contentId=:contentId";
		getSession().createQuery(sql).setParameter("contentId", contentId).executeUpdate();
	}

}
