package com.jeecms.cms.dao.main;

import com.jeecms.cms.entity.main.ContentExtMedia;

public interface ContentExtMediaDao {
	
	public ContentExtMedia save(ContentExtMedia contentExtMedia);
	
	public ContentExtMedia update(ContentExtMedia contentExtMedia);
	
	public ContentExtMedia findById(Integer id);
	
	public ContentExtMedia deleteById(Integer id);
	
	public void deleteByContentId(Integer contentId);
}
