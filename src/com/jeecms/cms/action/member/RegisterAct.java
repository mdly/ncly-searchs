package com.jeecms.cms.action.member;

import com.jeecms.cms.entity.assist.CmsWebservice;
import com.jeecms.cms.manager.assist.CmsWebserviceMng;
import com.jeecms.common.email.EmailSender;
import com.jeecms.common.email.MessageTemplate;
import com.jeecms.common.web.RequestUtils;
import com.jeecms.common.web.ResponseUtils;
import com.jeecms.common.web.session.SessionProvider;
import com.jeecms.common.web.springmvc.MessageResolver;
import com.jeecms.core.entity.*;
import com.jeecms.core.entity.ext.LendBook;
import com.jeecms.core.entity.ext.UnifiedUserExt;
import com.jeecms.core.manager.CmsConfigItemMng;
import com.jeecms.core.manager.CmsUserMng;
import com.jeecms.core.manager.ConfigMng;
import com.jeecms.core.manager.UnifiedUserMng;
import com.jeecms.core.web.WebErrors;
import com.jeecms.core.web.util.CmsUtils;
import com.jeecms.core.web.util.FrontUtils;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jeecms.cms.Constants.TPLDIR_MEMBER;

/**
 * 前台会员注册Action
 */
@Controller
public class RegisterAct {
	private static final Logger log = LoggerFactory
			.getLogger(RegisterAct.class);

	public static final String REGISTER = "tpl.register";
	public static final String REGISTER_RESULT = "tpl.registerResult";
	public static final String REGISTER_ACTIVE_SUCCESS = "tpl.registerActiveSuccess";
	public static final String LOGIN_INPUT = "tpl.loginInput";

	public static final String REGISTER_IN_2 = "tpl.registerIn2";//跳至注册院内读者步骤2页面
	public static final String REGISTER_IN_3 = "tpl.registerIn3";//跳至注册院内读者步骤3..
	public static final String REGISTER_OUT_2 = "tpl.registerOut2";//跳至注册院外读者步骤2..

	/**
	 * 参数绑定对象上
	 * @param binder
     */
	@InitBinder("unifiedUserExt")
	public void initBinder1(WebDataBinder binder) {
		binder.setFieldDefaultPrefix("u.");
	}


	@RequestMapping(value = "/register.jspx", method = RequestMethod.GET)
	public String input(HttpServletRequest request,
						HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		MemberConfig mcfg = site.getConfig().getMemberConfig();
		// 没有开启会员功能
		if (!mcfg.isMemberOn()) {
			return FrontUtils.showMessage(request, model, "member.memberClose");
		}
		// 没有开启会员注册
		if (!mcfg.isRegisterOn()) {
			return FrontUtils.showMessage(request, model,
					"member.registerClose");
		}
		List<CmsConfigItem>items=cmsConfigItemMng.getList(site.getConfig().getId(), CmsConfigItem.CATEGORY_REGISTER);
		FrontUtils.frontData(request, model, site);
		model.addAttribute("mcfg", mcfg);
		model.addAttribute("items", items);
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_MEMBER, REGISTER);
	}

	/**
	 * 跳至注册院内读者步骤2页面，返回步骤1的表单
	 * @param unifiedUserExt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/register_in_2.jspx", method = RequestMethod.POST)
	public String register_in_2(UnifiedUserExt unifiedUserExt,
								HttpServletRequest request, HttpServletResponse response,
								ModelMap model) throws IOException {
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		model.addAttribute("userExt",unifiedUserExt);
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_MEMBER, REGISTER_IN_2);
	}

	/**
	 * 跳至注册院内读者步骤3页面，返回步骤1和2的表单
	 * @param unifiedUserExt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/register_in_3.jspx", method = RequestMethod.POST)
	public String register_in_3(UnifiedUserExt unifiedUserExt,
								HttpServletRequest request, HttpServletResponse response,
								ModelMap model) throws IOException {
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		LendBook byLendBookNo = cmsUserMng.findByLendBookNo(unifiedUserExt.getLendBookNo());
		model.addAttribute("userExt",unifiedUserExt);
		Boolean flag = unifiedUserMng.lendBookNo_unique(unifiedUserExt.getLendBookNo());
		if(null!=byLendBookNo&&flag==false){
			//成功跳至注册3页面，返回步骤2所填入借书证号的个人信息对象
			model.addAttribute("lendbookno",byLendBookNo);
			return FrontUtils.getTplPath(request, site.getSolutionPath(),
					TPLDIR_MEMBER, REGISTER_IN_3);
		}
		if(null==byLendBookNo){
			//无此借书证号
			model.addAttribute("status",0);
			return FrontUtils.getTplPath(request, site.getSolutionPath(),
					TPLDIR_MEMBER, REGISTER_IN_2);
		}else if(flag){
			//此借书证号已被注册
			model.addAttribute("status",1);
			return FrontUtils.getTplPath(request, site.getSolutionPath(),
					TPLDIR_MEMBER, REGISTER_IN_2);
		}else{
			model.addAttribute("lendbookno",byLendBookNo);
			return FrontUtils.getTplPath(request, site.getSolutionPath(),
					TPLDIR_MEMBER, REGISTER_IN_3);
		}
	}

	/**
	 * 用户名唯一验证
	 * @param username
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/username_unique_v1.jspx", method = RequestMethod.POST)
	@ResponseBody
	public String usernameUnique(@RequestParam("username")String username, HttpServletRequest request, HttpServletResponse response) {
		// 用户名已存在，返回1
		if (unifiedUserMng.usernameExist(username)) {
			return "1";
		}
		return "0";
	}

	/**
	 * 邮箱唯一验证
	 * @param email
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/email_unique_v1.jspx", method = RequestMethod.POST)
	@ResponseBody
	public String emailUnique(@RequestParam("email")String email,HttpServletRequest request, HttpServletResponse response) {
		// email已被注册，返回1
		if (unifiedUserMng.emailExist(email)) {
			return "1";
		}
		return "0";
	}

	@RequestMapping(value = "/register_out_2.jspx", method = RequestMethod.POST)
	public String register_out_2(UnifiedUserExt unifiedUserExt,
								 HttpServletRequest request, HttpServletResponse response,
								 ModelMap model) throws IOException {
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		model.addAttribute("userExt",unifiedUserExt);
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_MEMBER, REGISTER_OUT_2);
	}

	/**
	 * 注册接口
	 * @param unifiedUserExt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws IOException
	 */
//	@ResponseBody
//	@RequestMapping(value = "/register_v1.jspx", method = RequestMethod.POST)
	public String submit(UnifiedUserExt unifiedUserExt, HttpServletRequest request, HttpServletResponse response,
						 ModelMap model)throws IOException {
		String ip = RequestUtils.getIpAddr(request);
		CmsSite site = CmsUtils.getSite(request);
		unifiedUserExt.setRegisterIp(ip);
		Integer i = cmsUserMng.registerMember_v1(unifiedUserExt);
		return i.toString();
	}

	@ResponseBody
	@RequestMapping(value = "/register_v1.jspx", method = RequestMethod.POST)
	public Map submit_(UnifiedUserExt unifiedUserExt,
						  CmsUserExt userExt, String captcha, String nextUrl,
						  HttpServletRequest request, HttpServletResponse response,
						  ModelMap model) throws IOException {
		Map<String,String> map = new HashMap();
		CmsSite site = CmsUtils.getSite(request);
		CmsConfig config = site.getConfig();
		String username = unifiedUserExt.getUsername();
		String email = unifiedUserExt.getEmail();
		String password = unifiedUserExt.getPassword();

		if(unifiedUserMng.usernameExist(username)){
			//用户名已存在
			map.put("status","0");
			return map;
		}
		if(unifiedUserMng.emailExist(email)){
			//邮箱已存在
			map.put("status","1");
			return map;
		}

		if(unifiedUserMng.lendBookNo_unique(unifiedUserExt.getLendBookNo())){
			//借书证号不可重复注册
			map.put("status","2");
			return map;
		}

		boolean disabled=false;
		if(config.getMemberConfig().isCheckOn()){
			disabled=true;
		}
		String ip = RequestUtils.getIpAddr(request);
		Map<String,String>attrs=RequestUtils.getRequestMap(request, "attr_");
		Boolean emailValidate = config.getEmailValidate();
		emailValidate = false;//关闭邮箱验证和激活步骤
		if (emailValidate) {
			EmailSender sender = configMng.getEmailSender();
			MessageTemplate msgTpl = configMng.getRegisterMessageTemplate();
			if (sender == null) {
				// 邮件服务器没有设置好
				map.put("status", "4");
			} else if (msgTpl == null) {
				// 邮件模板没有设置好
				map.put("status", "5");
			} else {
				try {
					cmsUserMng.registerMember(unifiedUserExt,ip,null,disabled,userExt,attrs, false, sender, msgTpl);
					cmsWebserviceMng.callWebService("false",username, password, email, userExt,CmsWebservice.SERVICE_TYPE_ADD_USER);
					//注册成功提示激活邮箱
					map.put("status", "200");
				} catch (UnsupportedEncodingException e) {
					// 发送邮件异常
					map.put("status", "100");
					map.put("message", e.getMessage());
					log.error("send email exception.", e);
				} catch (MessagingException e) {
					// 发送邮件异常
					map.put("status", "101");
					map.put("message", e.getMessage());
					log.error("send email exception.", e);
				}
			}
			log.info("member register success. username={}", username);
			FrontUtils.frontData(request, model, site);
			if (!StringUtils.isBlank(nextUrl)) {
				response.sendRedirect(nextUrl);
				return null;
			} else {
				return map;
			}
		} else {
			cmsUserMng.registerMember(unifiedUserExt, ip, null,null,disabled,userExt,attrs);
			cmsWebserviceMng.callWebService("false",username, password, email, userExt,CmsWebservice.SERVICE_TYPE_ADD_USER);
			log.info("member register success. username={}", username);
			FrontUtils.frontData(request, model, site);
			FrontUtils.frontPageData(request, model);
			//注册成功，但跳过邮箱验证激活步骤
			map.put("status", "201");
			return map;
		}
	}

	@RequestMapping(value = "/register.jspx", method = RequestMethod.POST)
	public String submit_(String username, String email, String password,
						  CmsUserExt userExt, String captcha, String nextUrl,
						  HttpServletRequest request, HttpServletResponse response,
						  ModelMap model) throws IOException {
		CmsSite site = CmsUtils.getSite(request);
		CmsConfig config = site.getConfig();
		WebErrors errors = validateSubmit(username, email, password, captcha,
				site, request, response);
		boolean disabled=false;
		if(config.getMemberConfig().isCheckOn()){
			disabled=true;
		}
		if (errors.hasErrors()) {
			return FrontUtils.showError(request, response, model, errors);
		}
		String ip = RequestUtils.getIpAddr(request);
		Map<String,String>attrs=RequestUtils.getRequestMap(request, "attr_");
		if (config.getEmailValidate()) {
			EmailSender sender = configMng.getEmailSender();
			MessageTemplate msgTpl = configMng.getRegisterMessageTemplate();
			if (sender == null) {
				// 邮件服务器没有设置好
				model.addAttribute("status", 4);
			} else if (msgTpl == null) {
				// 邮件模板没有设置好
				model.addAttribute("status", 5);
			} else {
				try {
					cmsUserMng.registerMember(username, email, password,ip,null,disabled,userExt,attrs, false, sender, msgTpl);
					cmsWebserviceMng.callWebService("false",username, password, email, userExt,CmsWebservice.SERVICE_TYPE_ADD_USER);
					model.addAttribute("status", 0);
				} catch (UnsupportedEncodingException e) {
					// 发送邮件异常
					model.addAttribute("status", 100);
					model.addAttribute("message", e.getMessage());
					log.error("send email exception.", e);
				} catch (MessagingException e) {
					// 发送邮件异常
					model.addAttribute("status", 101);
					model.addAttribute("message", e.getMessage());
					log.error("send email exception.", e);
				}
			}
			log.info("member register success. username={}", username);
			FrontUtils.frontData(request, model, site);
			if (!StringUtils.isBlank(nextUrl)) {
				response.sendRedirect(nextUrl);
				return null;
			} else {
				return FrontUtils.getTplPath(request, site.getSolutionPath(),
						TPLDIR_MEMBER, REGISTER_RESULT);
			}
		} else {
			cmsUserMng.registerMember(username, email, password, ip, null,null,disabled,userExt,attrs);
			cmsWebserviceMng.callWebService("false",username, password, email, userExt,CmsWebservice.SERVICE_TYPE_ADD_USER);
			log.info("member register success. username={}", username);
			FrontUtils.frontData(request, model, site);
			FrontUtils.frontPageData(request, model);
			model.addAttribute("success", true);
			return FrontUtils.getTplPath(request, site.getSolutionPath(),
					TPLDIR_MEMBER, LOGIN_INPUT);
		}
	}

	@RequestMapping(value = "/active.jspx", method = RequestMethod.GET)
	public String active(String username, String key,
						 HttpServletRequest request, HttpServletResponse response,
						 ModelMap model) throws IOException {
		CmsSite site = CmsUtils.getSite(request);
		username = new String(request.getParameter("username").getBytes("iso8859-1"),"GBK");
		WebErrors errors = validateActive(username, key, request, response);
		if (errors.hasErrors()) {
			return FrontUtils.showError(request, response, model, errors);
		}
		UnifiedUser user = unifiedUserMng.active(username, key);
		FrontUtils.frontData(request, model, site);
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_MEMBER, REGISTER_ACTIVE_SUCCESS);
	}

	@RequestMapping(value = "/username_unique.jspx")
	public void usernameUnique(HttpServletRequest request,
							   HttpServletResponse response) {
		String username = RequestUtils.getQueryParam(request, "username");
		// 用户名为空，返回false。
		if (StringUtils.isBlank(username)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		CmsSite site = CmsUtils.getSite(request);
		CmsConfig config = site.getConfig();
		// 保留字检查不通过，返回false。
		if (!config.getMemberConfig().checkUsernameReserved(username)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		// 用户名存在，返回false。
		if (unifiedUserMng.usernameExist(username)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		ResponseUtils.renderJson(response, "true");
	}

	@RequestMapping(value = "/email_unique.jspx")
	public void emailUnique(HttpServletRequest request,
							HttpServletResponse response) {
		String email = RequestUtils.getQueryParam(request, "email");
		// email为空，返回false。
		if (StringUtils.isBlank(email)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		// email存在，返回false。
		if (unifiedUserMng.emailExist(email)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		ResponseUtils.renderJson(response, "true");
	}

	private WebErrors validateSubmit(String username, String email,
									 String password, String captcha, CmsSite site,
									 HttpServletRequest request, HttpServletResponse response) {
		MemberConfig mcfg = site.getConfig().getMemberConfig();
		WebErrors errors = WebErrors.create(request);
		try {
			if (!imageCaptchaService.validateResponseForID(session
					.getSessionId(request, response), captcha)) {
				errors.addErrorCode("error.invalidCaptcha");
				return errors;
			}
		} catch (CaptchaServiceException e) {
			errors.addErrorCode("error.exceptionCaptcha");
			log.warn("", e);
			return errors;
		}
		if (errors.ifOutOfLength(username,MessageResolver.getMessage(request, "field.username"),
				mcfg.getUsernameMinLen(), 100)) {
			return errors;
		}
		if (errors.ifNotUsername(username,MessageResolver.getMessage(request, "field.username"),
				mcfg.getUsernameMinLen(), 100)) {
			return errors;
		}
		if (errors.ifOutOfLength(password, MessageResolver.getMessage(request, "field.password"),
				mcfg.getPasswordMinLen(), 100)) {
			return errors;
		}
		if (errors.ifNotEmail(email, MessageResolver.getMessage(request, "field.email"), 100)) {
			return errors;
		}
		// 保留字检查不通过，返回false。
		if (!mcfg.checkUsernameReserved(username)) {
			errors.addErrorCode("error.usernameReserved");
			return errors;
		}
		// 用户名存在，返回false。
		if (unifiedUserMng.usernameExist(username)) {
			errors.addErrorCode("error.usernameExist");
			return errors;
		}
		return errors;
	}

	private WebErrors validateActive(String username, String activationCode,
									 HttpServletRequest request, HttpServletResponse response) {
		WebErrors errors = WebErrors.create(request);
		if (StringUtils.isBlank(username)
				|| StringUtils.isBlank(activationCode)) {
			errors.addErrorCode("error.exceptionParams");
			return errors;
		}
		UnifiedUser user = unifiedUserMng.getByUsername(username);
		if (user == null) {
			errors.addErrorCode("error.usernameNotExist");
			return errors;
		}
		/*
		 * firefox访问链接二次访问，现简单不验证
		if (user.getActivation()
				|| StringUtils.isBlank(user.getActivationCode())) {
			errors.addErrorCode("error.usernameActivated");
			return errors;
		}
		if (!user.getActivationCode().equals(activationCode)) {
			errors.addErrorCode("error.exceptionActivationCode");
			return errors;
		}
		*/
		if (StringUtils.isNotBlank(user.getActivationCode())&&!user.getActivationCode().equals(activationCode)) {
			errors.addErrorCode("error.exceptionActivationCode");
			return errors;
		}
		return errors;
	}

	@Autowired
	private CmsWebserviceMng cmsWebserviceMng;
	@Autowired
	private UnifiedUserMng unifiedUserMng;
	@Autowired
	private CmsUserMng cmsUserMng;
	@Autowired
	private SessionProvider session;
	@Autowired
	private ImageCaptchaService imageCaptchaService;
	@Autowired
	private ConfigMng configMng;
	@Autowired
	private CmsConfigItemMng cmsConfigItemMng;
}
