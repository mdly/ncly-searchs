package com.jeecms.cms.entity.main;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.jeecms.cms.entity.main.base.BaseContentExtMedia;

public class ContentExtMedia extends BaseContentExtMedia implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7140693356816694249L;

	public ContentExtMedia(Integer id, Integer contentId, String title, String mediaPath, String mediaType,
			String contentImg, int seqNum) {
		super(id, contentId, title, mediaPath, mediaType, contentImg, seqNum);
	}

	public ContentExtMedia(java.lang.Integer id) {
		super(id);
		initialize();
	}
	
	public ContentExtMedia() {
		super();
	}
	
	public void blankToNull() {
		if (StringUtils.isBlank(getTitle())) {
			setTitle(null);
		}

		if (StringUtils.isBlank(getMediaType())) {
			setMediaType(null);
		}
		if (StringUtils.isBlank(getContentImg())) {
			setContentImg(null);
		}
	}

}
