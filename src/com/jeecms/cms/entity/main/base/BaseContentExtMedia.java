package com.jeecms.cms.entity.main.base;

public abstract class BaseContentExtMedia {

	public static String REF = "ContentTag";
	public static String PROP_NAME = "name";
	public static String PROP_ID = "id";
	public static String PROP_CONTENTID = "contentId";
	public static String PROP_TITLE = "title";
	public static String PROP_MEDIAPATH = "mediaPath";
	public static String PROP_CONTENTIMG = "contentImg";
	public static String PROP_SEQNUM = "seqNum";

	private Integer id;
	private Integer contentId;
	private String title;
	private String mediaPath;
	private String mediaType;
	private String contentImg;
	private int seqNum;

	private int hashCode = Integer.MIN_VALUE;
	
	public BaseContentExtMedia () {
		initialize();
	}
	
	public BaseContentExtMedia(Integer id) {
		this.setId(id);
		initialize();
	}

	public BaseContentExtMedia(Integer id, Integer contentId, String title, String mediaPath, String mediaType,
			String contentImg, int seqNum) {
		this.setId(id);
		this.setContentId(contentId);
		this.setTitle(title);
		this.setMediaPath(mediaPath);
		this.setMediaType(mediaType);
		this.setContentImg(contentImg);
		this.setSeqNum(seqNum);
		initialize();
	}
	
	protected void initialize() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getContentId() {
		return contentId;
	}

	public void setContentId(Integer contentId) {
		this.contentId = contentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public String getContentImg() {
		return contentImg;
	}

	public void setContentImg(String contentImg) {
		this.contentImg = contentImg;
	}

	public int getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}

	public boolean equals(Object obj) {
		if (null == obj)
			return false;
		if (!(obj instanceof com.jeecms.cms.entity.main.ContentExtMedia))
			return false;
		else {
			com.jeecms.cms.entity.main.ContentExtMedia contentExtMedia = (com.jeecms.cms.entity.main.ContentExtMedia) obj;
			if (null == this.getId() || null == contentExtMedia.getId())
				return false;
			else
				return (this.getId().equals(contentExtMedia.getId()));
		}
	}

	public int hashCode() {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId())
				return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}

	public String toString() {
		return super.toString();
	}
}
