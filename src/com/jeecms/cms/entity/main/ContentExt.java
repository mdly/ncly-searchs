package com.jeecms.cms.entity.main;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jeecms.cms.entity.main.base.BaseContentExt;

public class ContentExt extends BaseContentExt {
	private static final long serialVersionUID = 1L;
	/**
	 * 视频集
	 */
	private java.util.List<com.jeecms.cms.entity.main.ContentExtMedia> medias;
	
	private String[] m_titles;
	
	private String[] m_mediaPath;
	
	private String[] m_seqNum;
	
	
	public String[] getM_titles() {
		java.util.List<com.jeecms.cms.entity.main.ContentExtMedia> list = getMedias();
		if(null == list || list.isEmpty()){
			return null;
		}
		m_titles = new String[list.size()];
		int index = 0;
		for(com.jeecms.cms.entity.main.ContentExtMedia contentExtMedia : list){
			m_titles[index++] = contentExtMedia.getTitle();
		}
		return m_titles;
	}

	public void setM_titles(String[] m_titles) {
		this.m_titles = m_titles;
	}

	public String[] getM_mediaPath() {
		java.util.List<com.jeecms.cms.entity.main.ContentExtMedia> list = getMedias();
		if(null == list || list.isEmpty()){
			return null;
		}
		m_mediaPath = new String[list.size()];
		int index = 0;
		for(com.jeecms.cms.entity.main.ContentExtMedia contentExtMedia : list){
			m_mediaPath[index++] = contentExtMedia.getMediaPath();
		}
		return m_mediaPath;
	}

	public void setM_mediaPath(String[] m_mediaPath) {
		this.m_mediaPath = m_mediaPath;
	}

	public String[] getM_seqNum() {
		java.util.List<com.jeecms.cms.entity.main.ContentExtMedia> list = getMedias();
		if(null == list || list.isEmpty()){
			return null;
		}
		m_seqNum = new String[list.size()];
		int index = 0;
		for(com.jeecms.cms.entity.main.ContentExtMedia contentExtMedia : list){
			m_seqNum[index++] = contentExtMedia.getSeqNum()+"";
		}
		return m_seqNum;
	}

	public void setM_seqNum(String[] m_seqNum) {
		this.m_seqNum = m_seqNum;
	}

	public java.util.List<com.jeecms.cms.entity.main.ContentExtMedia> getMedias() {
		return medias;
	}

	public void setMedias(java.util.List<com.jeecms.cms.entity.main.ContentExtMedia> medias) {
		this.medias = medias;
	}

	public void add2Medias(String title,String mediaPath,Integer seqNum){
		List<com.jeecms.cms.entity.main.ContentExtMedia> list = getMedias();
		if( null == list ){
			list = new ArrayList<com.jeecms.cms.entity.main.ContentExtMedia>();
		}
		com.jeecms.cms.entity.main.ContentExtMedia  contentExtMedia = new com.jeecms.cms.entity.main.ContentExtMedia();
		contentExtMedia.setTitle(title);
		contentExtMedia.setMediaPath(mediaPath);
		contentExtMedia.setSeqNum(seqNum);
		list.add(contentExtMedia);
	}
	
	
	/**
	 * 如果简短标题为空，则返回标题
	 * 
	 * @return
	 */
	public String getStitle() {
		if (!StringUtils.isBlank(getShortTitle())) {
			return getShortTitle();
		} else {
			return getTitle();
		}
	}

	public void init() {
		if (getReleaseDate() == null) {
			setReleaseDate(new Timestamp(System.currentTimeMillis()));
		}
		if (getBold() == null) {
			setBold(false);
		}
		if(getNeedRegenerate()==null){
			setNeedRegenerate(true);
		}
		blankToNull();
	}

	public void blankToNull() {
		if (StringUtils.isBlank(getShortTitle())) {
			setShortTitle(null);
		}
		if (StringUtils.isBlank(getAuthor())) {
			setAuthor(null);
		}
		if (StringUtils.isBlank(getOrigin())) {
			setOrigin(null);
		}
		if (StringUtils.isBlank(getOriginUrl())) {
			setOriginUrl(null);
		}
		if (StringUtils.isBlank(getDescription())) {
			setDescription(null);
		}
		if (StringUtils.isBlank(getTitleColor())) {
			setTitleColor(null);
		}
		if (StringUtils.isBlank(getTitleImg())) {
			setTitleImg(null);
		}
		if (StringUtils.isBlank(getContentImg())) {
			setContentImg(null);
		}
		if (StringUtils.isBlank(getTypeImg())) {
			setTypeImg(null);
		}
		if (StringUtils.isBlank(getLink())) {
			setLink(null);
		}
		if (StringUtils.isBlank(getTplContent())) {
			setTplContent(null);
		}
		if (StringUtils.isBlank(getMediaPath())) {
			setMediaPath(null);
		}
		if (StringUtils.isBlank(getMediaType())) {
			setMediaType(null);
		}
	}

	/* [CONSTRUCTOR MARKER BEGIN] */
	public ContentExt () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public ContentExt (java.lang.Integer id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public ContentExt (
		java.lang.Integer id,
		java.lang.String title,
		java.util.Date releaseDate,
		java.lang.Boolean bold,
		java.lang.Boolean needRegenerate) {

		super (
			id,
			title,
			releaseDate,
			bold,
			needRegenerate);
	}

	/* [CONSTRUCTOR MARKER END] */

}