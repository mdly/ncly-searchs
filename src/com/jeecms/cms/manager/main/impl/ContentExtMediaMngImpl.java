package com.jeecms.cms.manager.main.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.jeecms.cms.dao.main.ContentExtMediaDao;
import com.jeecms.cms.entity.main.ContentExtMedia;
import com.jeecms.cms.manager.main.ContentExtMediaMng;

public class ContentExtMediaMngImpl implements ContentExtMediaMng {

	@Override
	public ContentExtMedia save(ContentExtMedia contentExtMedia) {
		return contentExtMediaDao.save(contentExtMedia);
	}

	@Override
	public ContentExtMedia update(ContentExtMedia contentExtMedia) {
		return contentExtMediaDao.update(contentExtMedia);
	}

	@Override
	public ContentExtMedia findById(Integer id) {
		return contentExtMediaDao.findById(id);
	}

	@Override
	public ContentExtMedia deleteById(Integer id) {
		return contentExtMediaDao.deleteById(id);
	}

	public ContentExtMediaDao contentExtMediaDao;
	
	@Autowired
	public void setContentExtMediaDao(ContentExtMediaDao contentExtMediaDao) {
		this.contentExtMediaDao = contentExtMediaDao;
	}

	@Override
	public List<ContentExtMedia> save(List<ContentExtMedia> medias) {
		
		if(null == medias){
			return null;
		}
		for(ContentExtMedia contentExtMedia : medias){
			contentExtMediaDao.save(contentExtMedia);
		}
		
		return medias;
	}

	@Override
	public void deleteByContentId(Integer contentId) {
		contentExtMediaDao.deleteByContentId(contentId);
	}
	
}
