package com.jeecms.cms.manager.main.impl;

import com.jeecms.cms.dao.main.ContentDao;
import com.jeecms.cms.entity.assist.CmsFile;
import com.jeecms.cms.entity.main.Channel;
import com.jeecms.cms.entity.main.Content;
import com.jeecms.cms.entity.main.ContentCheck;
import com.jeecms.cms.entity.main.ContentCount;
import com.jeecms.cms.entity.main.ContentDoc;
import com.jeecms.cms.entity.main.ContentExt;
import com.jeecms.cms.entity.main.ContentExtMedia;
import com.jeecms.cms.entity.main.ContentShareCheck;
import com.jeecms.cms.entity.main.ContentTxt;
import com.jeecms.cms.manager.assist.CmsCommentMng;
import com.jeecms.cms.manager.assist.CmsFileMng;
import com.jeecms.cms.manager.main.ChannelMng;
import com.jeecms.cms.manager.main.CmsTopicMng;
import com.jeecms.cms.manager.main.ContentCheckMng;
import com.jeecms.cms.manager.main.ContentCountMng;
import com.jeecms.cms.manager.main.ContentDocMng;
import com.jeecms.cms.manager.main.ContentExtMediaMng;
import com.jeecms.cms.manager.main.ContentExtMng;
import com.jeecms.cms.manager.main.ContentMng;
import com.jeecms.cms.manager.main.ContentShareCheckMng;
import com.jeecms.cms.manager.main.ContentTagMng;
import com.jeecms.cms.manager.main.ContentTxtMng;
import com.jeecms.cms.manager.main.ContentTypeMng;
import com.jeecms.cms.service.ChannelDeleteChecker;
import com.jeecms.cms.service.ContentListener;
import com.jeecms.cms.staticpage.StaticPageSvc;
import com.jeecms.cms.staticpage.exception.ContentNotCheckedException;
import com.jeecms.cms.staticpage.exception.GeneratedZeroStaticPageException;
import com.jeecms.cms.staticpage.exception.StaticPageNotOpenException;
import com.jeecms.cms.staticpage.exception.TemplateNotFoundException;
import com.jeecms.cms.staticpage.exception.TemplateParseException;
import com.jeecms.common.hibernate3.Updater;
import com.jeecms.common.page.Pagination;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.entity.CmsUser;
import com.jeecms.core.entity.CmsUserSite;
import com.jeecms.core.entity.CmsWorkflow;
import com.jeecms.core.entity.CmsWorkflowEvent;
import com.jeecms.core.manager.CmsGroupMng;
import com.jeecms.core.manager.CmsUserMng;
import com.jeecms.core.manager.CmsWorkflowEventMng;
import com.jeecms.core.manager.CmsWorkflowMng;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@Transactional
public class ContentMngImpl
  implements ContentMng, ChannelDeleteChecker
{
  private List<ContentListener> listenerList;
  private ChannelMng channelMng;
  private ContentExtMng contentExtMng;

  @Autowired
  private ContentExtMediaMng contentExtMediaMng;
  private ContentTxtMng contentTxtMng;
  private ContentTypeMng contentTypeMng;
  private ContentCountMng contentCountMng;
  private ContentCheckMng contentCheckMng;
  private ContentTagMng contentTagMng;
  private CmsGroupMng cmsGroupMng;
  private CmsUserMng cmsUserMng;
  private CmsTopicMng cmsTopicMng;
  private CmsCommentMng cmsCommentMng;
  private ContentDao dao;
  private StaticPageSvc staticPageSvc;
  private CmsFileMng fileMng;
  private ContentShareCheckMng contentShareCheckMng;

  @Autowired
  private ContentDocMng contentDocMng;

  @Autowired
  private CmsWorkflowMng workflowMng;

  @Autowired
  private CmsWorkflowEventMng workflowEventMng;

  @Transactional(readOnly=true)
  public Pagination getPageByRight(String title, Integer typeId, Integer currUserId, Integer inputUserId, boolean topLevel, boolean recommend, Content.ContentStatus status, Byte checkStep, Integer siteId, Integer channelId, Integer userId, int orderBy, int pageNo, int pageSize)
  {
    CmsUser user = this.cmsUserMng.findById(userId);
    CmsUserSite us = user.getUserSite(siteId);

    boolean allChannel = us.getAllChannel().booleanValue();
    boolean selfData = user.getSelfAdmin().booleanValue();
    Integer departId = null;
    if (user.getDepartment() != null)
      departId = user.getDepartment().getId();
    Pagination p;
    if ((allChannel) && (selfData))
    {
      p = this.dao.getPageBySelf(title, typeId, inputUserId, topLevel, recommend, status, checkStep, siteId, channelId, 
        userId, orderBy, pageNo, pageSize);
    }
    else
    {
      if ((allChannel) && (!selfData))
      {
        p = this.dao.getPage(title, typeId, currUserId, inputUserId, topLevel, recommend, status, checkStep, siteId, 
          null, channelId, orderBy, pageNo, pageSize);
      }
      else p = this.dao.getPageByRight(title, typeId, currUserId, inputUserId, topLevel, recommend, status, checkStep, 
          siteId, channelId, departId, userId, selfData, orderBy, pageNo, pageSize);
    }
    return p;
  }

  public Pagination getPageBySite(String title, Integer typeId, Integer inputUserId, boolean topLevel, boolean recommend, Content.ContentStatus status, Integer siteId, int orderBy, int pageNo, int pageSize)
  {
    return this.dao.getPage(title, typeId, null, inputUserId, topLevel, recommend, status, null, siteId, null, null, 
      orderBy, pageNo, pageSize);
  }

  public Pagination getPageForMember(String title, Integer channelId, Integer siteId, Integer modelId, Integer memberId, int pageNo, int pageSize)
  {
    return this.dao.getPage(title, null, memberId, memberId, false, false, Content.ContentStatus.all, null, siteId, modelId, 
      channelId, 0, pageNo, pageSize);
  }

  @Transactional(readOnly=true)
  public List<Content> getExpiredTopLevelContents(byte topLevel, Date expiredDay) {
    return this.dao.getExpiredTopLevelContents(topLevel, expiredDay);
  }

  @Transactional(readOnly=true)
  public List<Content> getPigeonholeContents(Date pigeonholeDay) {
    return this.dao.getPigeonholeContents(pigeonholeDay);
  }

  @Transactional(readOnly=true)
  public Content getSide(Integer id, Integer siteId, Integer channelId, boolean next) {
    return this.dao.getSide(id, siteId, channelId, next, true);
  }

  @Transactional(readOnly=true)
  public List<Content> getListByIdsForTag(Integer[] ids, int orderBy) {
    if (ids.length == 1) {
      Content content = findById(ids[0]);
      List list;
      if (content != null) {
        list = new ArrayList(1);
        list.add(content);
      } else {
        list = new ArrayList(0);
      }
      return list;
    }
    return this.dao.getListByIdsForTag(ids, orderBy);
  }

  @Transactional(readOnly=true)
  public Pagination getPageBySiteIdsForTag(Integer[] siteIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, int pageNo, int pageSize)
  {
    return this.dao.getPageBySiteIdsForTag(siteIds, typeIds, titleImg, recommend, title, open, attr, orderBy, pageNo, 
      pageSize);
  }

  @Transactional(readOnly=true)
  public List<Content> getListBySiteIdsForTag(Integer[] siteIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, Integer first, Integer count)
  {
    return this.dao.getListBySiteIdsForTag(siteIds, typeIds, titleImg, recommend, title, open, attr, orderBy, first, 
      count);
  }

  @Transactional(readOnly=true)
  public Pagination getPageByChannelIdsForTag(Integer[] channelIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, int option, int pageNo, int pageSize)
  {
    return this.dao.getPageByChannelIdsForTag(channelIds, typeIds, titleImg, recommend, title, open, attr, orderBy, 
      option, pageNo, pageSize);
  }

  @Transactional(readOnly=true)
  public List<Content> getListByChannelIdsForTag(Integer[] channelIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, int option, Integer first, Integer count)
  {
    return this.dao.getListByChannelIdsForTag(channelIds, typeIds, titleImg, recommend, title, open, attr, orderBy, 
      option, first, count);
  }

  @Transactional(readOnly=true)
  public Pagination getPageByChannelPathsForTag(String[] paths, Integer[] siteIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, int pageNo, int pageSize)
  {
    return this.dao.getPageByChannelPathsForTag(paths, siteIds, typeIds, titleImg, recommend, title, open, attr, orderBy, 
      pageNo, pageSize);
  }

  @Transactional(readOnly=true)
  public List<Content> getListByChannelPathsForTag(String[] paths, Integer[] siteIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, Integer first, Integer count)
  {
    return this.dao.getListByChannelPathsForTag(paths, siteIds, typeIds, titleImg, recommend, title, open, attr, orderBy, 
      first, count);
  }

  @Transactional(readOnly=true)
  public Pagination getPageByTopicIdForTag(Integer topicId, Integer[] siteIds, Integer[] channelIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, int pageNo, int pageSize)
  {
    return this.dao.getPageByTopicIdForTag(topicId, siteIds, channelIds, typeIds, titleImg, recommend, title, open, attr, 
      orderBy, pageNo, pageSize);
  }

  @Transactional(readOnly=true)
  public List<Content> getListByTopicIdForTag(Integer topicId, Integer[] siteIds, Integer[] channelIds, Integer[] typeIds, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, Integer first, Integer count)
  {
    return this.dao.getListByTopicIdForTag(topicId, siteIds, channelIds, typeIds, titleImg, recommend, title, open, attr, 
      orderBy, first, count);
  }

  @Transactional(readOnly=true)
  public Pagination getPageByTagIdsForTag(Integer[] tagIds, Integer[] siteIds, Integer[] channelIds, Integer[] typeIds, Integer excludeId, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, int pageNo, int pageSize)
  {
    return this.dao.getPageByTagIdsForTag(tagIds, siteIds, channelIds, typeIds, excludeId, titleImg, recommend, title, 
      open, attr, orderBy, pageNo, pageSize);
  }

  @Transactional(readOnly=true)
  public List<Content> getListByTagIdsForTag(Integer[] tagIds, Integer[] siteIds, Integer[] channelIds, Integer[] typeIds, Integer excludeId, Boolean titleImg, Boolean recommend, String title, int open, Map<String, String[]> attr, int orderBy, Integer first, Integer count)
  {
    return this.dao.getListByTagIdsForTag(tagIds, siteIds, channelIds, typeIds, excludeId, titleImg, recommend, title, 
      open, attr, orderBy, first, count);
  }

  @Transactional(readOnly=true)
  public Content findById(Integer id) {
    Content entity = this.dao.findById(id);
    return entity;
  }

  public Content save(Content bean, ContentExt ext, ContentTxt txt, ContentDoc doc, Integer[] channelIds, Integer[] topicIds, Integer[] viewGroupIds, String[] tagArr, String[] attachmentPaths, String[] attachmentNames, String[] attachmentFilenames, String[] picPaths, String[] picDescs, Integer channelId, Integer typeId, Boolean draft, Boolean contribute, CmsUser user, boolean forMember, String[] m_titles, String[] m_mediaPaths, String[] m_seqNums)
  {
    saveContent(bean, ext, txt, doc, channelId, typeId, draft, contribute, user, forMember);
    int i;
    int len;
    ContentExtMedia contentExtMedia;
    if ((m_mediaPaths != null) && (m_mediaPaths.length > 0)) {
      List medias = new ArrayList();
      i = 0; for (len = m_mediaPaths.length; i < len; i++) {
        contentExtMedia = new ContentExtMedia();
        contentExtMedia.setTitle(m_titles[i]);
        contentExtMedia.setMediaPath(m_mediaPaths[i]);
        contentExtMedia.setSeqNum(Integer.parseInt(m_seqNums[i]));
        contentExtMedia.setContentId(ext.getId());
        medias.add(this.contentExtMediaMng.save(contentExtMedia));
      }
      ext.setMedias(medias);
    } else {
      ext.setMedias(null);
    }

    if ((channelIds != null) && (channelIds.length > 0)) {
      for (Integer cid : channelIds) {
        bean.addToChannels(this.channelMng.findById(cid));
      }
    }

    bean.addToChannels(this.channelMng.findById(channelId));

    if ((topicIds != null) && (topicIds.length > 0)) {
      for (Integer tid : topicIds) {
        if ((tid != null) && (tid.intValue() != 0)) {
          bean.addToTopics(this.cmsTopicMng.findById(tid));
        }
      }
    }

    if ((viewGroupIds != null) && (viewGroupIds.length > 0)) {
      for (Integer gid : viewGroupIds) {
        bean.addToGroups(this.cmsGroupMng.findById(gid));
      }
    }

    List tags = this.contentTagMng.saveTags(tagArr);
    bean.setTags(tags);

    if ((attachmentPaths != null) && (attachmentPaths.length > 0)) {
      i = 0; for (len = attachmentPaths.length; i < len; i++) {
        if (!StringUtils.isBlank(attachmentPaths[i])) {
          bean.addToAttachmemts(attachmentPaths[i], attachmentNames[i], attachmentFilenames[i]);
        }
      }

    }

    if ((picPaths != null) && (picPaths.length > 0)) {
      i = 0; for (len = picPaths.length; i < len; i++) {
        if (!StringUtils.isBlank(picPaths[i])) {
          bean.addToPictures(picPaths[i], picDescs[i]);
        }
      }

    }

    afterSave(bean);
    return bean;
  }

  public Content save(Content bean, ContentExt ext, ContentTxt txt, ContentDoc doc, Integer channelId, Integer typeId, Boolean draft, CmsUser user, boolean forMember)
  {
    saveContent(bean, ext, txt, doc, channelId, typeId, draft, Boolean.valueOf(false), user, forMember);

    afterSave(bean);
    return bean;
  }

  private Content saveContent(Content bean, ContentExt ext, ContentTxt txt, ContentDoc doc, Integer channelId, Integer typeId, Boolean draft, Boolean contribute, CmsUser user, boolean forMember)
  {
    Channel channel = this.channelMng.findById(channelId);
    bean.setChannel(channel);
    bean.setType(this.contentTypeMng.findById(typeId));
    bean.setUser(user);
    Byte userStep;
    if (forMember)
    {
      userStep = Byte.valueOf((byte)0);
    } else {
      CmsSite site = bean.getSite();
      userStep = user.getCheckStep(site.getId());
    }
    CmsWorkflow workflow = null;

    if ((contribute != null) && (contribute.booleanValue())) {
      bean.setStatus(Byte.valueOf((byte)4));
    } else if ((draft != null) && (draft.booleanValue()))
    {
      bean.setStatus(Byte.valueOf((byte)0));
    } else {
      workflow = channel.getWorkflowExtends();
      if (workflow != null)
        bean.setStatus(Byte.valueOf((byte)1));
      else {
        bean.setStatus(Byte.valueOf((byte)2));
      }

    }

    bean.setHasTitleImg(Boolean.valueOf(!StringUtils.isBlank(ext.getTitleImg())));
    bean.init();

    preSave(bean);
    this.dao.save(bean);
    this.contentExtMng.save(ext, bean);
    this.contentTxtMng.save(txt, bean);
    if (doc != null) {
      this.contentDocMng.save(doc, bean);
    }
    ContentCheck check = new ContentCheck();
    check.setCheckStep(userStep);
    if ((!forMember) && (workflow != null)) {
      int step = this.workflowMng.check(workflow, user, user, Integer.valueOf(Content.DATA_CONTENT), bean.getId(), null);
      if (step >= 0) {
        bean.setStatus(Byte.valueOf((byte)1));
        check.setCheckStep(Byte.valueOf((byte)step));
      } else {
        bean.setStatus(Byte.valueOf((byte)2));
      }
    }
    this.contentCheckMng.save(check, bean);
    this.contentCountMng.save(new ContentCount(), bean);
    return bean;
  }

  public Content update(Content bean, ContentExt ext, ContentTxt txt, ContentDoc doc, String[] tagArr, Integer[] channelIds, Integer[] topicIds, Integer[] viewGroupIds, String[] attachmentPaths, String[] attachmentNames, String[] attachmentFilenames, String[] picPaths, String[] picDescs, Map<String, String> attr, Integer channelId, Integer typeId, Boolean draft, CmsUser user, boolean forMember, String[] m_titles, String[] m_mediaPaths, String[] m_seqNums)
  {
    Content entity = findById(bean.getId());

    List mapList = preChange(entity);

    Updater updater = new Updater(bean);
    bean = this.dao.updateByUpdater(updater);
    Byte userStep;
    if (forMember)
    {
      userStep = Byte.valueOf((byte)0);
    } else {
      CmsSite site = bean.getSite();
      userStep = user.getCheckStep(site.getId());
    }
    Channel.AfterCheckEnum after = bean.getChannel().getAfterCheckEnum();

    if (after == Channel.AfterCheckEnum.BACK_UPDATE) {
      reject(bean.getId(), user, "");
    }

    if (draft != null) {
      if (draft.booleanValue()) {
        bean.setStatus(Byte.valueOf((byte)0));
      }
      else if (bean.getStatus().byteValue() == 0) {
        if (bean.getCheckStep().byteValue() >= bean.getChannel().getFinalStepExtends().byteValue())
          bean.setStatus(Byte.valueOf((byte)2));
        else {
          bean.setStatus(Byte.valueOf((byte)1));
        }
      }

    }

    processContentShareCheck(bean);

    bean.setHasTitleImg(Boolean.valueOf(!StringUtils.isBlank(ext.getTitleImg())));

    if (channelId != null) {
      bean.setChannel(this.channelMng.findById(channelId));
    }

    if (typeId != null) {
      bean.setType(this.contentTypeMng.findById(typeId));
    }

    this.contentExtMng.update(ext);

    this.contentTxtMng.update(txt, bean);

    if (doc != null) {
      this.contentDocMng.update(doc, bean);
    }

    if (attr != null) {
      Map attrOrig = bean.getAttr();
      attrOrig.clear();
      attrOrig.putAll(attr);
    }

    Set channels = bean.getChannels();
    channels.clear();
    if ((channelIds != null) && (channelIds.length > 0)) {
      for (Integer cid : channelIds) {
        channels.add(this.channelMng.findById(cid));
      }
    }
    channels.add(bean.getChannel());

    Set topics = bean.getTopics();
    topics.clear();
    if ((topicIds != null) && (topicIds.length > 0)) {
      for (Integer tid : topicIds) {
        if ((tid != null) && (tid.intValue() != 0)) {
          topics.add(this.cmsTopicMng.findById(tid));
        }
      }
    }

    Set groups = bean.getViewGroups();
    groups.clear();
    if ((viewGroupIds != null) && (viewGroupIds.length > 0)) {
      for (Integer gid : viewGroupIds) {
        groups.add(this.cmsGroupMng.findById(gid));
      }
    }

    this.contentTagMng.updateTags(bean.getTags(), tagArr);

    bean.getAttachments().clear();
    if ((attachmentPaths != null) && (attachmentPaths.length > 0)) {
      int i = 0; for (int len = attachmentPaths.length; i < len; i++) {
        if (!StringUtils.isBlank(attachmentPaths[i])) {
          bean.addToAttachmemts(attachmentPaths[i], attachmentNames[i], attachmentFilenames[i]);
        }
      }

    }

    if (bean.getContentExt().getMedias() != null) {
      this.contentExtMediaMng.deleteByContentId(ext.getId());
      bean.getContentExt().getMedias().clear();
    }

    if ((m_mediaPaths != null) && (m_mediaPaths.length > 0)) {
      Object medias = new ArrayList();
      int i = 0; for (int len = m_mediaPaths.length; i < len; i++) {
        ContentExtMedia contentExtMedia = new ContentExtMedia();
        contentExtMedia.setTitle(m_titles[i]);
        contentExtMedia.setMediaPath(m_mediaPaths[i]);
        contentExtMedia.setSeqNum(Integer.parseInt(m_seqNums[i]));
        contentExtMedia.setContentId(ext.getId());
        ((List)medias).add(this.contentExtMediaMng.save(contentExtMedia));
      }
      ext.setMedias((List)medias);
    }

    bean.getPictures().clear();
    if ((picPaths != null) && (picPaths.length > 0)) {
      int i = 0; for (int len = picPaths.length; i < len; i++) {
        if (!StringUtils.isBlank(picPaths[i])) {
          bean.addToPictures(picPaths[i], picDescs[i]);
        }
      }
    }

    afterChange(bean, mapList);
    return bean;
  }

  public Content update(Content bean) {
    Updater updater = new Updater(bean);
    bean = this.dao.updateByUpdater(updater);
    return bean;
  }

  public Content updateByChannelIds(Integer contentId, Integer[] channelIds) {
    Content bean = findById(contentId);
    Set channels = bean.getChannels();
    if ((channelIds != null) && (channelIds.length > 0))
    {
      for (Integer cid : channelIds) {
        channels.add(this.channelMng.findById(cid));
      }
    }
    return bean;
  }

  public Content addContentToTopics(Integer contentId, Integer[] topicIds) {
    Content bean = findById(contentId);
    Set topics = bean.getTopics();
    if ((topicIds != null) && (topicIds.length > 0)) {
      for (Integer tid : topicIds) {
        topics.add(this.cmsTopicMng.findById(tid));
      }
    }
    return bean;
  }

  public Content check(Integer id, CmsUser user) {
    Content content = findById(id);

    List mapList = preChange(content);
    ContentCheck check = content.getContentCheck();

    CmsWorkflow workflow = content.getChannel().getWorkflowExtends();

    if (content.getStatus().equals(Byte.valueOf((byte)2))) {
      return content;
    }
    int workflowstep = this.workflowMng.check(workflow, content.getUser(), user, Integer.valueOf(Content.DATA_CONTENT), content.getId(), 
      null);
    if (workflowstep == -1) {
      content.setStatus(Byte.valueOf((byte)2));

      check.setCheckOpinion(null);
      check.setRejected(Boolean.valueOf(false));
      check.setCheckStep(Byte.valueOf((byte)workflowstep));

      check.setReviewer(user);
      check.setCheckDate(Calendar.getInstance().getTime());
    } else if (workflowstep > 0) {
      content.setStatus(Byte.valueOf((byte)1));

      check.setCheckOpinion(null);
      check.setRejected(Boolean.valueOf(false));
      check.setCheckStep(Byte.valueOf((byte)workflowstep));
    }
    processContentShareCheck(content);

    afterChange(content, mapList);
    return content;
  }

  public Content[] check(Integer[] ids, CmsUser user) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = check(ids[i], user);
    }
    return beans;
  }

  public Content reject(Integer id, CmsUser user, String opinion) {
    Content content = findById(id);

    List mapList = preChange(content);

    ContentCheck check = content.getContentCheck();
    if ((content.getStatus().equals(Byte.valueOf((byte)1))) || (content.getStatus().equals(Byte.valueOf((byte)2)))) {
      CmsWorkflow workflow = content.getChannel().getWorkflow();
      int workflowstep = this.workflowMng.reject(workflow, content.getUser(), user, Integer.valueOf(Content.DATA_CONTENT), 
        content.getId(), opinion);

      if (workflowstep == -1) {
        content.setStatus(Byte.valueOf((byte)-1));
        check.setCheckStep(Byte.valueOf((byte)workflowstep));
        check.setCheckOpinion(opinion);
        check.setRejected(Boolean.valueOf(true));
      } else if (workflowstep > 0) {
        content.setStatus(Byte.valueOf((byte)1));
        check.setCheckStep(Byte.valueOf((byte)workflowstep));
        check.setCheckOpinion(opinion);
        check.setRejected(Boolean.valueOf(true));
      }
    }
    processContentShareCheck(content);

    afterChange(content, mapList);
    return content;
  }

  public Content[] reject(Integer[] ids, CmsUser user, String opinion) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = reject(ids[i], user, opinion);
    }
    return beans;
  }

  public Content submit(Integer id, CmsUser user) {
    Content content = findById(id);
    ContentCheck check = content.getContentCheck();

    CmsWorkflow workflow = content.getChannel().getWorkflowExtends();
    int step = this.workflowMng.check(workflow, content.getUser(), user, Integer.valueOf(Content.DATA_CONTENT), content.getId(), null);
    if (step == -1) {
      content.setStatus(Byte.valueOf((byte)2));

      check.setCheckOpinion(null);
      check.setRejected(Boolean.valueOf(false));
      check.setCheckStep(Byte.valueOf((byte)step));

      check.setReviewer(user);
      check.setCheckDate(Calendar.getInstance().getTime());
    } else if (step > 0) {
      content.setStatus(Byte.valueOf((byte)1));
      check.setCheckOpinion(null);
      check.setRejected(Boolean.valueOf(false));
      check.setCheckStep(Byte.valueOf((byte)step));
    }
    return content;
  }

  public Content[] submit(Integer[] ids, CmsUser user) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = submit(ids[i], user);
    }
    return beans;
  }

  public Content cycle(Integer id) {
    Content content = findById(id);

    List mapList = preChange(content);
    content.setStatus(Byte.valueOf((byte)3));
    processContentShareCheck(content);

    afterChange(content, mapList);
    return content;
  }

  public Content[] cycle(Integer[] ids) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = cycle(ids[i]);
    }
    return beans;
  }

  public Content recycle(Integer id) {
    Content content = findById(id);

    List mapList = preChange(content);
    byte contentStep = content.getCheckStep().byteValue();
    byte finalStep = content.getChannel().getFinalStepExtends().byteValue();
    if ((contentStep >= finalStep) && (!content.getRejected().booleanValue()))
      content.setStatus(Byte.valueOf((byte)2));
    else {
      content.setStatus(Byte.valueOf((byte)1));
    }
    processContentShareCheck(content);

    afterChange(content, mapList);
    return content;
  }

  public Content[] recycle(Integer[] ids) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = recycle(ids[i]);
    }
    return beans;
  }

  public Content deleteById(Integer id) {
    Content bean = findById(id);

    preDelete(bean);

    this.contentTagMng.removeTags(bean.getTags());
    bean.getTags().clear();

    this.cmsCommentMng.deleteByContentId(id);

    this.fileMng.deleteByContentId(id);

    CmsWorkflowEvent event = this.workflowEventMng.find(Integer.valueOf(Content.DATA_CONTENT), id);
    if (event != null) {
      this.workflowEventMng.deleteById(event.getId());
    }
    bean.clear();
    bean = this.dao.deleteById(id);

    afterDelete(bean);
    return bean;
  }

  public Content[] deleteByIds(Integer[] ids) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = deleteById(ids[i]);
    }
    return beans;
  }

  public Content deleteByIdWithShare(Integer id, Integer siteId) {
    Content bean = findById(id);

    if (bean.getSiteId().equals(siteId))
    {
      preDelete(bean);

      this.contentTagMng.removeTags(bean.getTags());
      bean.getTags().clear();

      this.cmsCommentMng.deleteByContentId(id);

      this.fileMng.deleteByContentId(id);

      CmsWorkflowEvent event = this.workflowEventMng.find(Integer.valueOf(Content.DATA_CONTENT), id);
      if (event != null) {
        this.workflowEventMng.deleteById(event.getId());
      }

      this.contentExtMediaMng.deleteByContentId(id);
      bean.clear();
      bean = this.dao.deleteById(id);

      afterDelete(bean);
    }
    else {
      bean = deleteShare(id);
    }
    return bean;
  }

  public Content[] deleteByIdsWithShare(Integer[] ids, Integer siteId) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = deleteByIdWithShare(ids[i], siteId);
    }
    return beans;
  }

  public Content deleteShare(Integer id) {
    Content entity = findById(id);
    Set sets = entity.getContentShareCheckSet();
    Iterator it = sets.iterator();
    Byte status = Byte.valueOf((byte)0);
    while (it.hasNext()) {
      ContentShareCheck shareCheck = (ContentShareCheck)it.next();
      shareCheck.setShareValid(Boolean.valueOf(false));
      shareCheck.setCheckStatus(status);
      this.contentShareCheckMng.update(shareCheck);
    }
    return entity;
  }

  public Content[] deleteShares(Integer[] ids) {
    Content[] beans = new Content[ids.length];
    int i = 0; for (int len = ids.length; i < len; i++) {
      beans[i] = deleteShare(ids[i]);
    }
    return beans;
  }

  public Content[] contentStatic(Integer[] ids) throws TemplateNotFoundException, TemplateParseException, GeneratedZeroStaticPageException, StaticPageNotOpenException, ContentNotCheckedException
  {
    int count = 0;
    List list = new ArrayList();
    int i = 0; for (int len = ids.length; i < len; i++) {
      Content content = findById(ids[i]);
      try {
        if (!content.getChannel().getStaticContent().booleanValue()) {
          throw new StaticPageNotOpenException("content.staticNotOpen", Integer.valueOf(count), content.getTitle());
        }
        if (!content.isChecked()) {
          throw new ContentNotCheckedException("content.notChecked", Integer.valueOf(count), content.getTitle());
        }
        if (this.staticPageSvc.content(content)) {
          list.add(content);
          count++;
        }
      } catch (IOException e) {
        throw new TemplateNotFoundException("content.tplContentNotFound", Integer.valueOf(count), content.getTitle());
      } catch (TemplateException e) {
        throw new TemplateParseException("content.tplContentException", Integer.valueOf(count), content.getTitle());
      }
    }
    if (count == 0) {
      throw new GeneratedZeroStaticPageException("content.staticGenerated");
    }
    Content[] beans = new Content[count];
    return (Content[])list.toArray(beans);
  }

  public Pagination getPageForCollection(Integer siteId, Integer memberId, int pageNo, int pageSize) {
    return this.dao.getPageForCollection(siteId, memberId, pageNo, pageSize);
  }

  public void updateFileByContent(Content bean, Boolean valid)
  {
    Set files = bean.getFiles();
    Iterator it = files.iterator();
    while (it.hasNext()) {
      CmsFile tempFile = (CmsFile)it.next();
      tempFile.setFileIsvalid(valid.booleanValue());
      this.fileMng.update(tempFile);
    }
  }

  public String checkForChannelDelete(Integer channelId) {
    int count = this.dao.countByChannelId(channelId.intValue());
    if (count > 0) {
      return "content.error.cannotDeleteChannel";
    }
    return null;
  }

  private void processContentShareCheck(Content entity)
  {
    Set sets = entity.getContentShareCheckSet();
    Iterator it = sets.iterator();
    while (it.hasNext()) {
      ContentShareCheck shareCheck = (ContentShareCheck)it.next();
      if (entity.getStatus().equals(Byte.valueOf((byte)2)))
        shareCheck.setShareValid(Boolean.valueOf(true));
      else {
        shareCheck.setShareValid(Boolean.valueOf(false));
      }
      this.contentShareCheckMng.update(shareCheck);
    }
  }

  private void preSave(Content content) {
    if (this.listenerList != null)
      for (ContentListener listener : this.listenerList)
        listener.preSave(content);
  }

  private void afterSave(Content content)
  {
    if (this.listenerList != null)
      for (ContentListener listener : this.listenerList)
        listener.afterSave(content);
  }

  private List<Map<String, Object>> preChange(Content content)
  {
    if (this.listenerList != null) {
      int len = this.listenerList.size();
      List list = new ArrayList(len);
      for (ContentListener listener : this.listenerList) {
        list.add(listener.preChange(content));
      }
      return list;
    }
    return null;
  }

  private void afterChange(Content content, List<Map<String, Object>> mapList)
  {
    if (this.listenerList != null) {
      Assert.notNull(mapList);
      Assert.isTrue(mapList.size() == this.listenerList.size());
      int len = this.listenerList.size();

      for (int i = 0; i < len; i++) {
        ContentListener listener = (ContentListener)this.listenerList.get(i);
        listener.afterChange(content, (Map)mapList.get(i));
      }
    }
  }

  private void preDelete(Content content) {
    if (this.listenerList != null)
      for (ContentListener listener : this.listenerList)
        listener.preDelete(content);
  }

  private void afterDelete(Content content)
  {
    if (this.listenerList != null)
      for (ContentListener listener : this.listenerList)
        listener.afterDelete(content);
  }

  public void setListenerList(List<ContentListener> listenerList)
  {
    this.listenerList = listenerList;
  }

  @Autowired
  public void setChannelMng(ChannelMng channelMng)
  {
    this.channelMng = channelMng;
  }

  @Autowired
  public void setContentTypeMng(ContentTypeMng contentTypeMng) {
    this.contentTypeMng = contentTypeMng;
  }

  @Autowired
  public void setContentCountMng(ContentCountMng contentCountMng) {
    this.contentCountMng = contentCountMng;
  }

  @Autowired
  public void setContentExtMng(ContentExtMng contentExtMng) {
    this.contentExtMng = contentExtMng;
  }

  @Autowired
  public void setContentTxtMng(ContentTxtMng contentTxtMng) {
    this.contentTxtMng = contentTxtMng;
  }

  @Autowired
  public void setContentCheckMng(ContentCheckMng contentCheckMng) {
    this.contentCheckMng = contentCheckMng;
  }

  @Autowired
  public void setCmsTopicMng(CmsTopicMng cmsTopicMng) {
    this.cmsTopicMng = cmsTopicMng;
  }

  @Autowired
  public void setContentTagMng(ContentTagMng contentTagMng) {
    this.contentTagMng = contentTagMng;
  }

  @Autowired
  public void setCmsGroupMng(CmsGroupMng cmsGroupMng) {
    this.cmsGroupMng = cmsGroupMng;
  }

  @Autowired
  public void setCmsUserMng(CmsUserMng cmsUserMng) {
    this.cmsUserMng = cmsUserMng;
  }

  @Autowired
  public void setCmsCommentMng(CmsCommentMng cmsCommentMng) {
    this.cmsCommentMng = cmsCommentMng;
  }

  @Autowired
  public void setFileMng(CmsFileMng fileMng) {
    this.fileMng = fileMng;
  }

  @Autowired
  public void setContentShareCheckMng(ContentShareCheckMng contentShareCheckMng) {
    this.contentShareCheckMng = contentShareCheckMng;
  }

  @Autowired
  public void setDao(ContentDao dao) {
    this.dao = dao;
  }

  @Autowired
  public void setStaticPageSvc(StaticPageSvc staticPageSvc) {
    this.staticPageSvc = staticPageSvc;
  }
}