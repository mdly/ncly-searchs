package com.jeecms.cms.manager.main;

import java.util.List;

import com.jeecms.cms.entity.main.ContentExtMedia;

public interface ContentExtMediaMng {

	public ContentExtMedia save(ContentExtMedia contentExtMedia);
	
	public List<ContentExtMedia> save(List<ContentExtMedia> medias);
	
	public ContentExtMedia update(ContentExtMedia contentExtMedia);
	
	public ContentExtMedia findById(Integer id);
	
	public ContentExtMedia deleteById(Integer id);
	
	public void deleteByContentId(Integer contentId);
	
}
