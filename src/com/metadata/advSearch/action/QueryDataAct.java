package com.metadata.advSearch.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeecms.cms.entity.main.Content;
import com.metadata.advSearch.dao.CmsQueryDataDao;
import com.metadata.advSearch.entity.DBContent;
import com.metadata.advSearch.manager.CmsQueryDataMng;

/**
 * 根据前台所传参数查询数据库
 * @author liuy
 *
 */
@Controller
public class QueryDataAct {
	
	/**
	 * 根据前台的参数搜索数据库数据
	 * 
	 * @param request
	 * @return list
	 */
	@RequestMapping(value = "/queryData.jspx")
	@ResponseBody
	public List<DBContent> querydata(HttpServletRequest request) {
		
		List<DBContent> dbclist = new ArrayList<>();
		String title = request.getParameter("title");
		String conpoint = request.getParameter("conpoint");
		String typepoint = request.getParameter("typepoint");
		String syspoint = request.getParameter("syspoint");
		String id = request.getParameter("id");
//		if(StringUtils.isEmpty(title)){
//			title = null;
//		}
		List<Content> list = cmsQueryDataMng.getQueryData(title, conpoint, typepoint, syspoint, id);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		for (Content content : list) {
			Map<String, String> attrMap = content.getAttr();
			String dateString = formatter.format(content.getReleaseDate());
			dbclist.add(new DBContent(content.getContentExt().getTitle(), content.getContentExt().getDescription(), content.getContentExt().getOrigin(),
					content.getContentExt().getAuthor(), content.getContentExt().getOriginUrl(), content.getUrl(), content.getContentExt().getId(), dateString));
		}
		
		return dbclist;
	}
	
	/**
	 * 访问量存入数据库
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/accessAmount.jspx")
	@ResponseBody
	public void accessAmount(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		String title = request.getParameter("title");
		cmsQueryDataMng.insert(id, title);
	}
	
	/**
	 * 访问量统计
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/count_views.jspx")
	@ResponseBody
	public List<Map<String,Object>> contentviews(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		List<Map<String,Object>> resultList =new ArrayList<Map<String,Object>>();
		resultList = cmsQueryDataMng.findListViews(id);
		return resultList;
	}

	/**
	 * 查询栏目下，内容的总数
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/channelContent_views.jspx")
	@ResponseBody
	public Integer channelContentviews(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		int view = cmsQueryDataDao.find(id);
		return view;
	}
	
	@RequestMapping(value = "/contentlable.jspx")
	@ResponseBody
	public String contentlable(HttpServletRequest request) {
		String str = cmsQueryDataDao.contentlable();
		return str;
	}
	
	@Autowired
	private CmsQueryDataMng cmsQueryDataMng;
	@Autowired
	private CmsQueryDataDao cmsQueryDataDao;
}
