package com.metadata.advSearch.entity;

import java.io.Serializable;

public class DBContent implements Serializable{
	
	//数据库标题
	private String title;
	
	//资源量
	private String description;
	
	//数据库链接
	private String origin;
	
	//资源条数
	private String author;
	
	//数据库链接
	private String originUrl;
		
	//路径
	private String url;
	
	//contentid
	private Integer id;
	
	//时间
	private String time;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getOriginUrl() {
		return originUrl;
	}

	public void setOriginUrl(String originUrl) {
		this.originUrl = originUrl;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	
	public DBContent(String title, String description, String origin, String author, String originUrl, String url,
			Integer id, String time) {
		super();
		this.title = title;
		this.description = description;
		this.origin = origin;
		this.author = author;
		this.originUrl = originUrl;
		this.url = url;
		this.id = id;
		this.time = time;
	}

	public DBContent() {
		super();
	}
	
}
