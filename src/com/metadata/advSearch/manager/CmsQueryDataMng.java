package com.metadata.advSearch.manager;

import java.util.List;
import java.util.Map;

import com.jeecms.cms.entity.main.Content;

public interface CmsQueryDataMng {

	public List<Content> getQueryData(String title, String conpoint, String typepoint, String syspoint, String id);
	
	public void insert(int id, String title);
	
	public List<Map<String,Object>>  findListViews(int id);
}
