package com.metadata.advSearch.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeecms.cms.entity.main.Content;
import com.metadata.advSearch.dao.CmsQueryDataDao;
import com.metadata.advSearch.manager.CmsQueryDataMng;
/**
 * 获取数据manager实现类
 * @author liuy
 *
 */
@Service
@Transactional
public class CmsQueryDataMngImpl implements CmsQueryDataMng{

	/**
	 * 获取数据
	 */
	@Override
	public List<Content> getQueryData(String title, String conpoint, String typepoint, String syspoint, String id) {
		List<Content> li = cmsQueryDataDao.getQueryData(title, conpoint, typepoint, syspoint, id);
		return li;
	}
	
	@Override
	public void insert(int id, String title) {
		cmsQueryDataDao.insert(id, title);
	}
	
	@Override
	public List<Map<String, Object>> findListViews(int id) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> resultListReal = getMonths();
		List<Object> cou = cmsQueryDataDao.findListViews(id);
		for(int i=0;i<cou.size();i++){
			Object[] obj = (Object[]) cou.get(i);
			String count = Integer.parseInt((String) obj[0])+"";
			String value = obj[1]+"";
			for(int j=0;j<resultListReal.size();j++){
				Map map2 = (Map) resultListReal.get(j);
				if(map2.get("months").equals(count)){
					map2.put("count", value);
					resultListReal.set(j, map2);
				}
			}
		}
		return resultListReal;
	}
	
	public List getMonths(){
		List list = new ArrayList<>();
		for(int i=1; i<13; i++){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("months", i+"");
			map.put("count", "0");
			list.add(map);
		}
		return list;
	}
	
	@Autowired
	private CmsQueryDataDao cmsQueryDataDao;

}
