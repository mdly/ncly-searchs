package com.metadata.advSearch.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.jeecms.cms.entity.assist.CmsAcquisition;
import com.jeecms.cms.entity.main.Content;
import com.jeecms.common.hibernate3.HibernateBaseDao;
import com.metadata.advSearch.dao.CmsQueryDataDao;

@Repository
public class CmsQueryDataDaoImpl extends HibernateBaseDao<CmsAcquisition, Integer> implements CmsQueryDataDao{

	@Override
	@SuppressWarnings("unchecked")
	public List<Content> getQueryData(String title, String conpoint, String typepoint, String syspoint, String id) {
		//中文数据库查询
		String sql = "select DISTINCT c.* from jc_content_ext a join jc_content_attr b on a.content_id = b.content_id left join jc_content c on c.content_id = a.content_id LEFT JOIN jc_channel d on c.channel_id = d.channel_id where ";
		if(StringUtils.isNotBlank(id)){
			sql+="  d.channel_id = " + id + " and  c.status=2";
		}else{
			sql+="  d.channel_id IN(233,234,235) and  c.status=2";
		}
		//判断为空，不为空则执行
		if(StringUtils.isNotBlank(title)){
			sql+= " and a.title like '%"+ title + "%' ";
		}
		if(StringUtils.isNotBlank(conpoint)){
			sql+= " and b.attr_name = 'conpoint' and b.attr_value like '%"+ conpoint + "%' ";
		}
		if(StringUtils.isNotBlank(typepoint)){
			sql+= " and b.attr_name = 'typepoint' and b.attr_value like '%"+ typepoint + "%' ";
		}
		if(StringUtils.isNotBlank(syspoint)){
			sql+= " and b.attr_name = 'syspoint' and b.attr_value like '%"+ syspoint + "%' ";
		}
		sql += " ORDER BY top_level DESC,";
		sql += " content_id DESC";
		List<Content> list = getSession().createSQLQuery(sql).addEntity(Content.class).list();
		return list;
	}

	@Override
	public void insert(int id, String title) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = dateFormat.format(new Date());
		String sql = "";
		Query query = null;
		sql = " INSERT INTO jc_group_view(content_id,content_title,time) ";
		sql += " VALUES ( ?, ?, STR_TO_DATE(?,'%Y-%m-%d %H:%i:%s')) ";
		query = getSession().createSQLQuery(sql);
		query.setInteger(0, id);
		query.setString(1, title);
		query.setString(2, date);
		query.executeUpdate();
	}
	
	@Override
	public List<Object> findListViews(int id) {
		String sql = "select DATE_FORMAT(time,'%m') months,count(*) count from jc_group_view  where content_id="+ id +" and time like '2016%' group by months "; 
		List list = getSession().createSQLQuery(sql).list();
		return list;
	}
	
	@Override
	public String findCountViews() {
		String sql = "SELECT SUM(views) FROM jc_channel_count"; 
		String count = getSession().createSQLQuery(sql).uniqueResult().toString();
		String sql1 = "SELECT SUM(views_day) FROM jc_channel_count";
		String count1 = getSession().createSQLQuery(sql1).uniqueResult().toString();
		String count2 = count +"/"+ count1;
		return count2;
	}

	@Override
	public int find(Integer id) {
		String sql = "SELECT count(*) FROM jc_content where channel_id=" +id; 
		List list = getSession().createSQLQuery(sql).list();
		if(list.size()>0){
			Integer i = Integer.parseInt(list.get(0).toString());
			return i;
		}else{
			return 0;
		}
	}

	@Override
	public String contentlable() {
		String sql = "SELECT opt_value FROM jc_model_item where model_id=11 and field = 'conpoint'"; 
		List list = getSession().createSQLQuery(sql).list();
		return list.get(0).toString();
	}

	@Override
	protected Class<CmsAcquisition> getEntityClass() {
		return null;
	}

}