package com.metadata.advSearch.dao;

import java.util.List;

import com.jeecms.cms.entity.main.Content;

public interface CmsQueryDataDao {
	
	/**
	 * 获取所有数据库数据
	 * @param title
	 * @param description
	 * @param origin
	 * @param id
	 * @return
	 */
	public List<Content> getQueryData(String title, String conpoint, String typepoint, String syspoint, String id);
	
	/**
	 * 存储点击量
	 * @param id
	 * @param title
	 */
	public void insert(int id,String title);
	
	/**
	 * 统计点击量返回到页面
	 * @return
	 */
	public List<Object>  findListViews(int id);
	
	/**
	 * 网站的总浏览量
	 * @return
	 */
	public String findCountViews();
	
	/**
	 * 查询栏目下，内容的总数
	 * @param id
	 * @return
	 */
	public int find(Integer id);
	
	/**
	 * 获取内容的标签字段
	 * @return
	 */
	public String contentlable();
	
}
