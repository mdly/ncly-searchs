package com.metadata.dowmload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jeecms.cms.action.front.AttachmentAct;
import com.jeecms.cms.entity.main.Content;
import com.jeecms.cms.entity.main.ContentAttachment;
import com.jeecms.cms.manager.main.ContentCountMng;
import com.jeecms.cms.manager.main.ContentMng;
import com.jeecms.common.web.ResponseUtils;
import com.jeecms.common.web.springmvc.RealPathResolver;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.web.WebErrors;
import com.jeecms.core.web.util.CmsUtils;

@Controller
public class AttachmentDown {
	private static final Logger log = LoggerFactory
			.getLogger(AttachmentAct.class);
	@Autowired
	private ContentMng contentMng;
	@Autowired
	private ContentCountMng contentCountMng;
	@Autowired
	private RealPathResolver realPathResolver;
	/**
	 * cid为内容id
	 * @param cid
	 * @param i  循环第几个
	 * @param request
	 * @param response
	 * @param model
	 * @throws IOException 
	 */
	@RequestMapping(value = "/metadate/attachment.jspx")
	public void attachmentDown(Integer cid, Integer i, HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws IOException{
		if(cid==null){
			ResponseUtils.renderText(response, "downlaod error!");
		}
		Content c = contentMng.findById(cid);
		if (c != null) {
			List<ContentAttachment> list = c.getAttachments();
			if (list.size() > i) {
				contentCountMng.downloadCount(c.getId());
				ContentAttachment ca = list.get(0);
				this.downLoad(request, response, ca);
			} else {
				log.info("download index is out of range: {}", i);
			}
		} else {
			log.info("Content not found: {}", cid);
		}
		
	}
	private void downLoad(HttpServletRequest request,
			HttpServletResponse response, ContentAttachment attachment) throws IOException {
		CmsSite site=CmsUtils.getSite(request);
		// 解决中文乱码
		String filename=attachment.getName();
		filename=encodeFilename(request, filename);
		//filename=encodeFilename(request, filename);
		// 文件相对路径
		String path = attachment.getPath();
		String ctx=site.getContextPath();
		if(StringUtils.isNotBlank(path)&&StringUtils.isNotBlank(ctx)){
			path=path.substring(ctx.length());
		}
		// 文件绝对路径
		String fileRealPath = realPathResolver.get(path);
		File file = new File(fileRealPath);
		if(file.exists()){
			byte[] byteArray = FileUtils.readFileToByteArray(file);
			response.addHeader("Content-Disposition", "attachment;filename="+filename);
			response.setContentType("application/x-msdownload");
			ServletOutputStream out = response.getOutputStream();
			out.write(byteArray);
			out.close();
		}else{
			ResponseUtils.renderJson(response, WebErrors.create(request).getMessage("content.doc.hasDelete"));
		}
	}
	public String encodeFilename(HttpServletRequest request,String fileName)  {
		  String agent = request.getHeader("USER-AGENT");
		  try {
		         // IE
		          if (null != agent && -1 != agent.indexOf("MSIE")) {
		                    fileName = URLEncoder.encode(fileName, "UTF8");
		           }else {
		        	   fileName = new String(fileName.getBytes("utf-8"),"iso-8859-1");
		           }
		  } catch (UnsupportedEncodingException e) {
			  e.printStackTrace();
		  }
		  return fileName;
	 }
	
	@RequestMapping("/metadata/down.jspx")  
	public void download(HttpServletResponse res,HttpServletRequest req) throws IOException {  
	    OutputStream os = res.getOutputStream();  
	    try {  
	        res.reset();  
	        String fileName = req.getParameter("fileName");
	        String path = req.getParameter("path");
	        res.setHeader("Content-Disposition", "attachment; filename="+""+new String(fileName.getBytes(),"ISO8859-1"));  
	        res.setContentType("application/octet-stream; charset=utf-8");  
	        OutputStream output = res.getOutputStream();
	        //String realPath=req.getSession().getServletContext().getRealPath("")+path;
	        String realPath = realPathResolver.get(path);
	        FileInputStream fis = new FileInputStream(realPath);
	        //设置每次写入缓存大小
	        byte[] b = new byte[5120];
	        //out.print(f.length());
	        //把输出流写入客户端
	        int i = 0;
	        while((i = fis.read(b)) > 0){
	          output.write(b, 0, i);
	        }
	        output.flush();
	    } finally {  
	        if (os != null) {  
	            os.close();  
	        }  
	    }  
	}
	
}
