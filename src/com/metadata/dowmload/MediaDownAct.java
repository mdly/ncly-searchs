package com.metadata.dowmload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jeecms.cms.entity.main.ContentAttachment;
import com.jeecms.cms.entity.main.ContentExt;
import com.jeecms.cms.entity.main.ContentExtMedia;
import com.jeecms.cms.manager.main.ContentExtMediaMng;
import com.jeecms.cms.manager.main.ContentMng;
import com.jeecms.common.web.ResponseUtils;
import com.jeecms.common.web.springmvc.RealPathResolver;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.web.WebErrors;
import com.jeecms.core.web.util.CmsUtils;

/**
 * 用于下载视频
 * 
 * @author Administrator
 *
 */
@Controller
public class MediaDownAct {
	/**
	 * 
	 * @param cid  获取内容id
	 * @param request
	 * @param response
	 * @param model
	 * @throws IOException 
	 */
	@RequestMapping(value="/metadata/downvedio.jspx")
	public void downMedia(Integer cid,Integer id,  HttpServletRequest request, HttpServletResponse response, ModelMap model) throws IOException {
		if (cid == null) {
			ResponseUtils.renderText(response, "downlaod error!");
		}
		ContentExt contentExt=contentMng.findById(cid).getContentExt();
		 ContentExtMedia contentExtMedia = this.contentExtMediaMng.findById(id);
		if(contentExt!=null){
			this.downLoad(request, response, contentExt,contentExtMedia);
		}
	}

	private void downLoad(HttpServletRequest request,
			HttpServletResponse response, ContentExt contentExt, ContentExtMedia contentExtMedia) throws IOException {
		CmsSite site=CmsUtils.getSite(request);
		// 解决中文乱码
			String path = contentExtMedia.getMediaPath();
		
		 	String filename = contentExt.getStitle();
		 	String type=path.substring(path.indexOf(".")+1, path.length());
		    String title = contentExtMedia.getTitle();
		    filename = "(" + filename + ")" + title + "." + type;
		    filename = encodeFilename(request, filename);
		    String ctx = site.getContextPath();
		    if ((StringUtils.isNotBlank(path)) && (StringUtils.isNotBlank(ctx))) {
		      path = path.substring(ctx.length());
		    }
		//文件的相对路径
		String fileRealPath = this.realPathResolver.get(path);
		File file = new File(fileRealPath);
		if(file.exists()){
			InputStream is = new FileInputStream(file);
			//byte[] byteArray = FileUtils.readFileToByteArray(file);//使用 FileUtils下载时有限制
			response.addHeader("Content-Disposition", "attachment;filename="+filename);
			response.setContentType("application/x-msdownload");
			ServletOutputStream out = response.getOutputStream();
			
			byte[] data = new byte[2048];
			  int len = 0;
			  while ((len = is.read(data)) > 0) {
			   out.write(data, 0, len);
			  }
			
			//out.write(byteArray);
			out.close();
		}else{
			ResponseUtils.renderJson(response, WebErrors.create(request).getMessage("content.vedio.hasDelete"));
		}
	}
	public String encodeFilename(HttpServletRequest request,String fileName)  {
		  String agent = request.getHeader("USER-AGENT");
		  try {
		         // IE
			  if (-1 != agent.indexOf("Chrome"))
		      {
		        fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
		      }
		      else
		        fileName = URLEncoder.encode(fileName, "UTF8");
		  } catch (UnsupportedEncodingException e) {
			  e.printStackTrace();
		  }
		  return fileName;
	 }

	@Autowired
	private ContentMng contentMng;
	 @Autowired
	 private ContentExtMediaMng contentExtMediaMng;
	@Autowired
	private RealPathResolver realPathResolver;
}
