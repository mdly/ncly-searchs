package com.metadata.search;

import static com.jeecms.cms.Constants.TPLDIR_SPECIAL;
import static com.jeecms.cms.Constants.TPLDIR_INDEX;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jeecms.cms.entity.main.Channel;
import com.jeecms.common.web.RequestUtils;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.web.util.CmsUtils;
import com.jeecms.core.web.util.FrontUtils;
import com.jeecms.plug.channelCollection.manager.ChannelListMng;

@Controller
public class SearchAction {
	public static final String SEARCH = "tpl.serach";
	public static final String INDEX = "tpl.index";
	
	public static final String SEARCH_CHANNEL="tpl.search_channel";
	@RequestMapping(value = "/metadata/search*.jspx")
	public String search(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		//获取需要查询的值
		String q=request.getParameter("q");
//		String q =RequestUtils.getQueryParam(request, "q");
		try {
			q=URLDecoder.decode(q,"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//获取select的值
		String category = RequestUtils.getQueryParam(request, "category");
		
		//获取channelid
		String channelId=RequestUtils.getQueryParam(request, "channelId");
		model.putAll(RequestUtils.getQueryParams(request));
		FrontUtils.frontData(request, model, site);
		FrontUtils.frontPageData(request, model);
		if(q==null||q.equals("")){
			return	FrontUtils.getTplPath(request, site.getSolutionPath(),TPLDIR_INDEX, INDEX);
			}
		if (StringUtils.isBlank(q)) {
			model.remove("q");
		} else {
			// 处理lucene查询字符串中的关键字
			String parseQ=parseKeywords(q);
			model.addAttribute("q",parseQ);
		}
		//model.addAttribute("input", q);
		if(category.equals("title")){
			model.addAttribute("queryTitle", q);
		}
		if(category.equals("txt")){
			model.addAttribute("queryTxt", q);
		}
		if(category.equals("description")){
			model.addAttribute("description", q);
		}
		if(category.equals("author")){
			model.addAttribute("author", q);
		}
		if(category.equals("keywords")){
			model.addAttribute("keywords", q);
		}
		if(category.equals("releaseDate")){
			model.addAttribute("releaseDate", q);
		}
		model.addAttribute("channelId", channelId);
//		if(category.equals("title")){
//			//先到所有栏目下去找，如果找到了就返回
//			List<Channel> list=this.channelListMng.getListChannel(q);
//			if(list!=null&&list.size()>0){
//				return FrontUtils.getTplPath(request, site.getSolutionPath(), TPLDIR_SPECIAL, SEARCH_CHANNEL);
//			}
//		}
		return FrontUtils.getTplPath(request, site.getSolutionPath(), TPLDIR_SPECIAL, SEARCH);
	}
	public static String parseKeywords(String q){
		char c='\\';
		int cIndex=q.indexOf(c);
		if(cIndex!=-1&&cIndex==0){
			q=q.substring(1);
		}
		if(cIndex!=-1&&cIndex==q.length()-1){
			q=q.substring(0,q.length()-1);
		}
		try {
			String regular = "[\\+\\-\\&\\|\\!\\(\\)\\{\\}\\[\\]\\^\\~\\*\\?\\:\\\\]";
			Pattern p = Pattern.compile(regular);
			Matcher m = p.matcher(q);
			String src = null;
			while (m.find()) {
				src = m.group();
				q = q.replaceAll("\\" + src, ("\\\\" + src));
			}
			q = q.replaceAll("AND", "and").replaceAll("OR", "or").replace("NOT", "not").replace("[", "［").replace("]", "］");
		} catch (Exception e) {
			e.printStackTrace();
			q=q;
		}
		return  q;
	}
	@Autowired
	private ChannelListMng channelListMng;
}
