package com.metadata.channel;

import static com.jeecms.cms.Constants.TPLDIR_CONTENT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jeecms.common.web.RequestUtils;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.web.util.CmsUtils;
import com.jeecms.core.web.util.FrontUtils;

/**
 * 此类主要用于将channel页转到内容页
 * 
 * @author xpy
 *
 */
@Controller
public class ChannelAction {
	public static final String SEARCH = "tpl.vedio";
	@RequestMapping(value = "/metadata/channel.jspx")
	public String channelAction(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		//获取channelid
		String channelId=RequestUtils.getQueryParam(request, "channelId");
		model.putAll(RequestUtils.getQueryParams(request));
		FrontUtils.frontData(request, model, site);
		FrontUtils.frontPageData(request, model);
		model.addAttribute("channelId", channelId);
		return FrontUtils.getTplPath(request, site.getSolutionPath(), TPLDIR_CONTENT, SEARCH);
	}
}
